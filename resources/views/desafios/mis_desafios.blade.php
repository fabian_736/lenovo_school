@extends('layouts.home.app')
@section('content')


    <div class="row pr-0 mr-0">
        <div class="col-1 d-flex justify-content-end align-items-center">
        <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
        </div>
        <div class="col">
            <div class="row">
                <div class="col">
                    <label for="" class="h3 titleback_desafios"><span style="font-weight: bold">MIS DESAFIOS</label>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="" class="h5 subtitledback_desafios">Acá encontrarás los desafios que has culminado satisfactoriamente</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row mx-auto d-flex justify-content-between row_firtscard">
        <div class="col col_desafios">
            <div class="backgroundDesafios">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row d-flex justify-content-center">
                        <div class="col cardgreen">
    
                        </div>
                    </div>
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <img src="{{url('svg/check.svg')}}" alt="" class="svgDiploma">
                        </div>
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio" >Descargar diploma</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col col_desafios">
            <div class="backgroundDesafios">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row d-flex justify-content-center">
                        <div class="col cardgreen">
    
                        </div>
                    </div>
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <img src="{{url('svg/check.svg')}}" alt="" class="svgDiploma">
                        </div>
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio" >Descargar diploma</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col col_desafios">
            <div class="backgroundDesafios">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row d-flex justify-content-center">
                        <div class="col cardgreen" >
    
                        </div>
                    </div>
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <img src="{{url('svg/check.svg')}}" alt="" class="svgDiploma">
                        </div>
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio" >Descargar diploma</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="row mx-auto d-flex justify-content-between">
        <div class="col col_desafios">
            <div class="backgroundDesafios">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row d-flex justify-content-center">
                        <div class="col cardgreen" >
    
                        </div>
                    </div>
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <img src="{{url('svg/check.svg')}}" alt="" class="svgDiploma">
                        </div>
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio" >Descargar diploma</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col col_desafios">
            <div class="backgroundDesafios">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row d-flex justify-content-center">
                        <div class="col cardgreen" >
    
                        </div>
                    </div>
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <img src="{{url('svg/check.svg')}}" alt="" class="svgDiploma">
                        </div>
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio" >Descargar diploma</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col col_desafios">
            <div class="backgroundDesafios">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row d-flex justify-content-center">
                        <div class="col cardgreen" >
    
                        </div>
                    </div>
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <img src="{{url('svg/check.svg')}}" alt="" class="svgDiploma">
                        </div>
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio" >Descargar diploma</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!--

    <div class="row pr-0 mr-0">
        <div class="col">
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                  
                  <li class="page-item"><a class="page-link" href="javascript:;">1</a></li>
                  <li class="page-item active"><a class="page-link" href="javascript:;">2</a></li>
                  <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                 
                </ul>
              </nav>
        </div>
    </div>

-->



  <!-- Modal HAS FINALIZADO LA CLASE -->
  <div class="modal fade" id="modal_two" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <img src="{{url('svg/users.svg')}}" alt="">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <h3 class="text-center" style="font-weight: bold">¡FELICIDADES!</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <label for="" class="lead text-center" style="color: black" >Has descargado éxitosamente el diploma</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <a href="javascript:;" data-dismiss="modal" aria-label="Close" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                        SALIR
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>


 <!-- Modal DIPLOMA -->
 <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-ku" role="document">
        <style>
            .modal-ku {
                width:100%;
                max-width:1000px
}
        </style>
      <div class="modal-content backgroundModal">
        <div class="modal-body " >
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-toggle="modal" data-target="#modal_two" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
               <div class="row">
                   <div class="col">
                       <div class="w-75 mx-auto diploma" style="border-radius: 20px; background-image: url('/img/diploma_data.png'); 
                       background-size: 100% 100%;
                       background-repeat: no-repeat;">

                       <!--
                            <div class="row-reverse ">
                                <div class="col d-flex justify-content-center p-0" style="margin-top: 10%">
                                    <img src="{{url('img/logo_blanco.png')}}" style="width: 18%" alt="" class="m-0 p-0">
                                </div>
                                <div class="col d-flex justify-content-center my-3">
                                    <label for="" class="lead text-white" style="font-weight: bold">Desafío ThinkShield</label>
                                </div>
                                <div class="col d-flex justify-content-center">
                                    <label for="" class="text-white" style="font-weight: bold">Javier Torres Bernal</label>
                                </div>
                                <div class="col d-flex justify-content-center px-5">
                                    <p class="mx-5 text-center text-white ">Lorem ipsum dolor sit, amet consectetur adipisicing elit.
                                        orem ipsum dolor sit, amet consectetur adipisicing elit.
                                    </p>
                                </div>
                                <div class="col d-flex justify-content-center mt-3">
                                    <label for="" class="fa-sm">21 de Noviembre de 2021</label>
                                </div>
                                <div class="col mt-5">
                                    <div class="row d-flex justify-content-between">
                                        <div class="col d-flex justify-content-center">
                                            <div class="row-reverse">
                                                <div class="col p-0 m-0">
                                                    <hr style="border-style: solid; border-width: 1px; border-color: white;">
                                                </div>
                                                <div class="col p-0 m-0">
                                                    <label for="" class="text-white fa-sm m-0" style="font-weight: bold">Nombre Apellido</label>
                                                </div>
                                                <div class="col p-0 m-0">
                                                    <p class="text-white text-center fa-sm m-0">Cargo</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col d-flex justify-content-center">
                                            <div class="row-reverse">
                                                <div class="col p-0 m-0">
                                                    <hr style="border-style: solid; border-width: 1px; border-color: white;">
                                                </div>
                                                <div class="col p-0 m-0">
                                                    <label for="" class="text-white fa-sm m-0" style="font-weight: bold">Nombre Apellido</label>
                                                </div>
                                                <div class="col p-0 m-0">
                                                    <p class="text-white text-center fa-sm m-0">Cargo</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        -->

                        <style>

                            @media (max-height: 1366px){
                                .diploma{
                                    min-height: 400px !important;
                                    max-width: 60% !important;
                                }
                            }
                        </style>

                       </div>
                   </div>
               </div>
               <div class="row my-3">
                    <div class="col d-flex justify-content-center align-items-center">
                        <a href="{{url('img/diploma_data.png')}}" download  class="btn" style="background: #8246AF; border-radius: 20px;">
                            DESCARGAR
                        </a>
                    </div>
                    <!-- 
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col d-flex justify-content-center my-2">
                                <span>COMPARTIR</span>
                            </div>
                            <div class="col d-flex justify-content-center">
                                <a href="javascript:;" class="mr-2">
                                    <img src="{{url('svg/network/facebook.png')}}" alt="">
                                </a>
                                <a href="javascript:;" class="mr-2">
                                    <img src="{{url('svg/network/instagram.png')}}" alt="">
                                </a>
                                <a href="javascript:;" class="mr-2">
                                    <img src="{{url('svg/network/linkedin.png')}}" alt="">
                                </a>
                                <a href="javascript:;" class="mr-2">
                                    <img src="{{url('svg/network/twitter.png')}}" alt="">
                                </a>
                            </div>
                    </div>
                -->
               </div>
            </div>
        </div>
      </div>
    </div>
  </div>




  <style>

      .cardgreen{
        position: absolute;
        background: #7DBE38;
        opacity: 0.7;
        max-width: 94%;
        min-height: 100%;
        border-radius: 20px;
      }

    .backgroundModal{
            background-image: url('/img/home.png');
            background-attachment: fixed;
            background-size: cover;
            background-repeat: no-repeat;
        }
    
        @media (min-width: 1920px){
                .backgroundDesafios{
                background-image: url('/img/desafios_2.png'); 
                    background-size:100% 100%;
                    background-repeat: no-repeat;
                    height: 230px;
                    border-radius: 20px !important;
            }
    
            .backgroundDesafiosInactive{
                background-image: url('/img/desafios_3.png'); 
                background-size:100% 100%;
                    background-repeat: no-repeat;
                    height: 230px;
                    border-radius: 20px !important;
            }
    
            .titleDesafio{
            font-weight: bold;
            }
    
            .row_firtscard{
                margin-top: 3%;
                margin-bottom: 5%;
            }
    
            .svgDiploma{
                width: 30% !important;
            }
    
            .row_svgSpecial{
                right: -30px !important;
            }
    
        }
    
    
        @media (max-width: 1366px){
                .backgroundDesafios{
                background-image: url('/img/desafios_2.png'); 
                background-size:100% 100%;
                    background-repeat: no-repeat;
                    height: 150px;
                    border-radius: 20px !important;
            }
    
            .backgroundDesafiosInactive{
                background-image: url('/img/desafios_3.png'); 
                background-size:100% 100%;
                    background-repeat: no-repeat;
                    height: 150px;
                    border-radius: 20px !important;
            }
    
            .titleDesafio{
            font-weight: bold;
            font-size: 1.3em;
            }
    
            .titleback_desafios{
                font-size: 1.2em;
            }
    
            .subtitledback_desafios{
                font-size: 1em;
            }
    
            .svgDiploma{
                width: 30% !important;
            }
           
            .row_firtscard{
                margin-top: 2%;
                margin-bottom: 5%;
            }

            .cardgreen{
                position: absolute;
                background: #7DBE38;
                opacity: 0.7;
                max-width: 90.5%;
                min-height: 100%;
                border-radius: 20px;
            }
        }

        @media (max-width: 720px){
        .col_desafios{
            min-width: 100% !important;
            margin-bottom: 5% !important;
        }

    }
        
    
    </style>


@endsection