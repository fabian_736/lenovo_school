<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name')->comment("Nombres");
            $table->string('lastname')->comment("Apellidos");
            $table->string('company')->comment("Empresa a la que pertenece");
            $table->string('position')->comment("Cargo");
            $table->string('channel')->comment("Canal");
            $table->string('cellphone_number')->comment("Número de teléfono");
            $table->string('token_reset')->nullable()->comment("token para reestablecimiento de contraseña");
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password')->nullable();
            $table->string('file')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
