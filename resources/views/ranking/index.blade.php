@extends('layouts.home.app')
@section('content')


<div class="row pr-0 mr-0">
    <div class="col-1 d-flex justify-content-end align-items-center">
       <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <label for="" class="h3 titleranking" style="font-weight: bold">RANKING</label>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="" class="h5 subtitledranking">Logra estar entre los primeros lugares de la tabla general asistiendo a tus sesiones y completando los desafíos Lenovo.
                </label>
            </div>
        </div>
    </div>
</div>


<div class="row mx-auto">
    <div class="col">
        <div class="card card-body cardprincipal" style="background-image: url('/img/background_perfilcard.png'); 
        background-attachment: fixed;
        background-size: cover;
        background-repeat: no-repeat; border-radius: 20px;">
            <div class="row">
                <div class="col-4 col_one">
                    <div class="card card-body p-0" style="border-radius: 20px;">
                        <div class="row-reverse">
                            <div class="col d-flex justify-content-center px-3 ">
                                <label for="" class="h3 text-center title_cardone" style="font-weight: bold">PRIMEROS LUGARES
                                    TABLA GENERAL</label>
                            </div>
                            <div class="col my-5 px-5 col_cardone">
                                <li class="row mb-4">
                                    <div class="col" style="background-image: url('/img/background_ranking_top.png'); 
                                    background-size: 100% 100%;
                                    background-repeat: no-repeat; border-top-left-radius: 40px; border-bottom-left-radius: 40px;">
                                        <div class="row">
                                            <div class="col-3 p-0 ">
                                                <div class="d-flex justify-content-center align-items-center div_img" style="background: #fff; width: 70px; height: 70px; border-radius: 40px; ">
                                                    <img src="{{url('img/user.png')}}" alt="" class="w-100">
                                                    <div style="position: absolute; width: 25px; height: 25px; background: red; top: 0; left: 0; border-radius: 40px;">
                                                        <img src="{{url('svg/country/peru.svg')}}" alt="" class="w-100">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-start align-items-center">
                                                <div class="row-reverse">
                                                    <div class="col ">
                                                        <label  for="" style="color: black;" class="name_cardone">Jose Rodriguéz</label> <br>
                                                        <label  for="" class="font-weight-bold last_cardone" style="color: rgb(255, 255, 255)">Vendedor</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-1 mr-1" style="position: absolute; width: 25px; height: 25px; background: #F5C930; top: 0; right: 0; border-radius: 40px;">
                                                <div class="row">
                                                    <div class="col ">
                                                        <div class="w-100 d-flex justify-content-center" style="border-radius: 40px">
                                                            <p class="font-weight-bold">1</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="row mb-4">
                                    <div class="col" style="background-image: url('/img/background_ranking_top.png'); 
                                    background-size: 100% 100%;
                                    background-repeat: no-repeat; border-top-left-radius: 40px; border-bottom-left-radius: 40px;">
                                        <div class="row">
                                            <div class="col-3 p-0 ">
                                                <div class="d-flex justify-content-center align-items-center div_img" style="background: #fff; width: 70px; height: 70px; border-radius: 40px; ">
                                                    <img src="{{url('img/user.png')}}" alt="" class="w-100">
                                                    <div style="position: absolute; width: 25px; height: 25px; background: red; top: 0; left: 0; border-radius: 40px;">
                                                        <img src="{{url('svg/country/peru.svg')}}" alt="" class="w-100">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-start align-items-center">
                                                <div class="row-reverse">
                                                    <div class="col ">
                                                        <label  for="" style="color: black" class="name_cardone">Jose Rodriguéz</label> <br>
                                                        <label  for="" class="font-weight-bold last_cardone" style="color: rgb(255, 255, 255)">Vendedor</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-1 mr-1" style="position: absolute; width: 25px; height: 25px; background: #CDCDCD; top: 0; right: 0; border-radius: 40px;">
                                                <div class="row">
                                                    <div class="col ">
                                                        <div class="w-100 d-flex justify-content-center" style="border-radius: 40px">
                                                            <p class="font-weight-bold">2</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li class="row mb-4">
                                    <div class="col" style="background-image: url('/img/background_ranking_top.png'); 
                                    background-size: 100% 100%;
                                    background-repeat: no-repeat; border-top-left-radius: 40px; border-bottom-left-radius: 40px;">
                                        <div class="row">
                                            <div class="col-3 p-0 ">
                                                <div class="d-flex justify-content-center align-items-center div_img" style="background: #fff; width: 70px; height: 70px; border-radius: 40px; ">
                                                    <img src="{{url('img/user.png')}}" alt="" class="w-100">
                                                    <div style="position: absolute; width: 25px; height: 25px; background: red; top: 0; left: 0; border-radius: 40px;">
                                                        <img src="{{url('svg/country/peru.svg')}}" alt="" class="w-100">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col d-flex justify-content-start align-items-center">
                                                <div class="row-reverse">
                                                    <div class="col ">
                                                        <label  for="" style="color: black" class="name_cardone">Jose Rodriguéz</label> <br>
                                                        <label  for="" class="font-weight-bold last_cardone" style="color: rgb(255, 255, 255)">Vendedor</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mt-1 mr-1" style="position: absolute; width: 25px; height: 25px; background: #CD7914; top: 0; right: 0; border-radius: 40px;">
                                                <div class="row">
                                                    <div class="col ">
                                                        <div class="w-100 d-flex justify-content-center" style="border-radius: 40px">
                                                            <p class="font-weight-bold">3</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-7 offset-md-1 col_second">
                    <div class="card card-body p-0 cardbody_second" style="border-radius: 20px;">
                      <div class="row">
                          <div class="col-3 d-flex justify-content-center align-items-center col_filtros" style="background: #E1E1E1; min-height: 50vh; max-height: 50vh; border-top-left-radius: 20px; border-bottom-left-radius: 20px;">
                              <div class="row-reverse">
                                  <div class="col d-flex justify-content-center align-items-center mb-5" >
                                      <label for="" class="h3" style="font-weight: bold">Filtros</label>
                                  </div>
                                  <div class="col " style="min-height: 25vh">
                                      <div class="row-reverse">
                                          <div class="col p-0">
                                              <a href="javascript:;" class="btn btn-block" onclick="open_one()" style="background: #7DBE38; border-radius: 20px">general</a>
                                          </div>
                                          <div class="col my-3 p-0">
                                            <a href="javascript:;" class="btn btn-block" onclick="open_two()" style="background: #7DBE38; border-radius: 20px">clases</a>
                                          </div>
                                          <div class="col p-0">
                                            <a href="javascript:;" class="btn btn-block" onclick="open_three()" style="background: #7DBE38; border-radius: 20px">desafios</a>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>

                          <!-- GENERAL -->

                            <div class="col p-3 filtro_one" style="border-top-right-radius: 20px; border-bottom-right-radius: 20px;">
                                <ul class="p-0 m-0">
                                    <div class="row mb-3">
                                        <div class="col-1 d-flex justify-content-center align-items-center p-0">
                                            <div class="d-flex justify-content-center align-items-center div_num" style="background: #999999; border-radius: 40px; width: 50px; height: 50px">
                                                <p class="m-0 p-0 lead text-white font-weight-bold">1</p>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <li style="list-style:none" >
                                                <div class="col" style="background-image: url('/img/background_items_racking.png'); 
                                                background-size: 100% 100%;
                                                background-repeat: no-repeat; border-top-left-radius: 40px; border-bottom-left-radius: 40px;">
                                                    <div class="row">
                                                        <div class="col-2 p-0 ">
                                                            <div class="d-flex justify-content-center align-items-center div_img" style="background: #fff; width: 70px; height: 70px; border-radius: 40px; ">
                                                                <img src="{{url('img/user.png')}}" alt="" class="w-100">
                                                            </div>
                                                        </div>
                                                        <div class="col d-flex justify-content-start align-items-center">
                                                            <div class="row-reverse">
                                                                <div class="col ">
                                                                    <label  for="" class="name_cardone" style="color: black">Jose Rodriguéz</label> <br>
                                                                    <label  for="" class="font-weight-bold last_cardone" style="color: rgb(255, 255, 255)">Vendedor</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col d-flex justify-content-end align-items-center">
                                                            <div class="w-25">
                                                                <img src="{{url('svg/country/peru.svg')}}" alt="" class="w-100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-1 d-flex justify-content-center align-items-center p-0">
                                            <div class="d-flex justify-content-center align-items-center div_num" style="background: #999999; border-radius: 40px; width: 50px; height: 50px">
                                                <p class="m-0 p-0 lead text-white font-weight-bold">2</p>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <li style="list-style:none" >
                                                <div class="col" style="background-image: url('/img/background_items_racking.png'); 
                                                background-size: 100% 100%;
                                                background-repeat: no-repeat; border-top-left-radius: 40px; border-bottom-left-radius: 40px;">
                                                    <div class="row">
                                                        <div class="col-2 p-0 ">
                                                            <div class="d-flex justify-content-center align-items-center div_img" style="background: #fff; width: 70px; height: 70px; border-radius: 40px; ">
                                                                <img src="{{url('img/user.png')}}" alt="" class="w-100">
                                                            </div>
                                                        </div>
                                                        <div class="col d-flex justify-content-start align-items-center">
                                                            <div class="row-reverse">
                                                                <div class="col ">
                                                                    <label  for="" style="color: black" class="name_cardone">Jose Rodriguéz</label> <br>
                                                                    <label  for="" class="font-weight-bold last_cardone" style="color: rgb(255, 255, 255)">Vendedor</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col d-flex justify-content-end align-items-center">
                                                            <div class="w-25">
                                                                <img src="{{url('svg/country/peru.svg')}}" alt="" class="w-100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-1 d-flex justify-content-center align-items-center p-0">
                                            <div class="d-flex justify-content-center align-items-center div_num" style="background: #999999; border-radius: 40px; width: 50px; height: 50px">
                                                <p class="m-0 p-0 lead text-white font-weight-bold">3</p>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <li style="list-style:none" >
                                                <div class="col" style="background-image: url('/img/background_items_racking.png'); 
                                                background-size: 100% 100%;
                                                background-repeat: no-repeat; border-top-left-radius: 40px; border-bottom-left-radius: 40px;">
                                                    <div class="row">
                                                        <div class="col-2 p-0 ">
                                                            <div class="d-flex justify-content-center align-items-center div_img" style="background: #fff; width: 70px; height: 70px; border-radius: 40px; ">
                                                                <img src="{{url('img/user.png')}}" alt="" class="w-100">
                                                            </div>
                                                        </div>
                                                        <div class="col d-flex justify-content-start align-items-center">
                                                            <div class="row-reverse">
                                                                <div class="col ">
                                                                    <label  for="" style="color: black" class="name_cardone">Jose Rodriguéz</label> <br>
                                                                    <label  for="" class="font-weight-bold last_cardone" style="color: rgb(255, 255, 255)">Vendedor</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col d-flex justify-content-end align-items-center">
                                                            <div class="w-25">
                                                                <img src="{{url('svg/country/peru.svg')}}" alt="" class="w-100">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        </div>
                                    </div>
                                </ul>
                            </div>

                        <!-- CLASES -->

                        <div class="col p-3 filtro_two" style="display: none; min-height: 30vh; max-height: 30vh; border-top-right-radius: 20px; border-bottom-right-radius: 20px;">
                            <div  style="overflow: auto; overflow-x: hidden; min-height: 45vh; max-height: 45vh;">
             
                            </div>
                        </div>

                        <!-- DESAFIOS -->

                        <div class="col p-3 filtro_three" style="display: none; min-height: 30vh; max-height: 30vh; border-top-right-radius: 20px; border-bottom-right-radius: 20px;">
                            <div  style="overflow: auto; overflow-x: hidden; min-height: 45vh; max-height: 45vh;">
                          
                            </div>
                        </div>

                      </div>
                    </div>
                </div>

                <div class="col col_movil" style="display: none">
                    <div class="card card-body p-0" style="border-radius: 20px;">
                        <div class="col d-flex justify-content-center align-items-center " style="background: #E1E1E1; min-height: 35vh; max-height: 35vh; border-top-left-radius: 20px; border-bottom-left-radius: 20px;">
                            <div class="row-reverse">
                                <div class="col d-flex justify-content-center align-items-center mt-5 mb-3" >
                                    <label for="" class="h3" style="font-weight: bold">Filtros</label>
                                </div>
                                <div class="col " style="min-height: 30vh">
                                    <div class="row-reverse">
                                        <div class="col p-0">
                                            <a href="javascript:;" class="btn btn-block" onclick="open_one()" style="background: #7DBE38; border-radius: 20px">general</a>
                                        </div>
                                        <div class="col my-3 p-0">
                                          <a href="javascript:;" class="btn btn-block" onclick="open_two()" style="background: #7DBE38; border-radius: 20px">clases</a>
                                        </div>
                                        <div class="col p-0">
                                          <a href="javascript:;" class="btn btn-block" onclick="open_three()" style="background: #7DBE38; border-radius: 20px">desafios</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>


<script>

    function open_one(){
        $('.filtro_one').show();
        $('.filtro_two').hide();
        $('.filtro_three').hide();
    }

    function open_two(){
        $('.filtro_one').hide();
        $('.filtro_two').show();
        $('.filtro_three').hide();
    }

    function open_three(){
        $('.filtro_one').hide();
        $('.filtro_two').hide();
        $('.filtro_three').show();
    }

</script>

<style>
    @media (max-width: 1366px){
        .titleranking{
            font-size: 1.2em;
        }
        .subtitledranking{
            font-size: 1em;
        }
        .title_cardone{
            font-size: 1em;
        }
        .name_cardone{
            font-size: 0.8em;
        }
        .last_cardone{
            font-size: 0.8em;
        }

        .div_img{
            width: 60px !important;
            height: 100% !important;
        }

        .col_cardone{
            margin-bottom: 0 !important;
            margin-top: 5% !important;
        }

        .col_second{
           min-width: 65% !important;
        }
        .div_num{
            width: 60px !important;
            height: 40px !important;
        }
        .cardprincipal{
            margin-bottom: 0 !important;
            padding-bottom: 0 !important;
            padding-top: 0 !important;
        }
    }

    @media (max-width: 1365px){
        .col_one{
           min-width: 100% !important;
        }

        .col_second{
           min-width: 100% !important;
        }

        .col_filtros{
            display: none !important;
        }

        .div_num{
            width: 30px !important;
            height: 30px !important;
        }

        .cardbody_second{
            border-left-color: #7DBE38;
            border-left-width: 5px;
            border-left-style: solid;
        }

        .col_movil{
            display: block !important;
        }

    }
</style>

<script>
    function query() {
      const mediaQuery = window.matchMedia('(max-width: 1366px)')
  
      if(mediaQuery.matches){
        $('.col_second').removeClass("offset-md-1");
      }
    }
  
    window.onload = query;
  
  </script>


@endsection