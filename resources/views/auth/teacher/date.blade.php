@extends('layouts.auth.app')
@section('content')

<body style=" background-image: url('/img/background_date_t.png');
background-repeat: no-repeat;
background-position: center;
background-size: cover;
width: 100%;
height:100vh;
padding: 5%;">


<div class="row">
    <div class="col-md-4 col-lg-4 col-sm-10 col-xs-10">
        <div class="card form p-5">
            <div class="row-reverse">
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <img src="{{url('img/logo.png')}}" alt="">
                </div>
                <div class="col d-flex justify-content-center align-items-center p-5">
                    <h3 for="" style="color: #7DBE38; font-family: 'Gotham', sans-serif;">Datos Adicionales</h3>
                </div>
                <form action="">
                    <div class="col">
                        <input type="text" id="date" class="form-control" placeholder="Nombres">
                    </div>
                    <div class="col">
                        <input type="text" id="date" class="form-control my-2" placeholder="Apellidos">
                    </div>
                    <div class="col">
                        <input type="text" id="date" class="form-control" placeholder="Télefono">
                    </div>
                    <div class="col">
                        <input type="text" id="date" class="form-control my-2" placeholder="Cargo">
                    </div>
                    <div class="col-8 mt-5 mb-3 mx-auto">
                            <div class="form-group">
                                <input type="file" name="file" id="file" class="input-file">
                                <label for="file" class="btn btn-tertiary js-labelFile d-flex align-items-center justify-content-center">
                                    <img src="{{url('svg/camara.svg')}}" alt="" class="mr-2">
                                  <span class="js-fileName">SUBIR FOTO</span>
                                </label>
                              </div>
                            
                    </div>
                    <div class="col-8 mx-auto">
                        <a href="{{route('teacher.date')}}" class="btn form-control text-white" style="border-radius: 20px; background: #7DBE38; font-size: 20px;">INGRESAR</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<style>

body {
  padding: 50px;
}
.btn-tertiary {
  padding: 0;
  line-height: 40px;
  margin: auto;
  display: block;
  border-radius: 20px;
  color: #7DBE38;
  background: white;
  border-width: 2px;
  border-color: #7DBE38;
  font-size: 20px;
 cursor: pointer;
}


.input-file {
	width: 0.1px;
	height: 0.1px;
	opacity: 0;
	overflow: hidden;
	position: absolute;
	z-index: -1;
  }

</style>

<script>
    (function() {
  
  'use strict';

  $('.input-file').each(function() {
    var $input = $(this),
        $label = $input.next('.js-labelFile'),
        labelVal = $label.html();
    
   $input.on('change', function(element) {
      var fileName = '';
      if (element.target.value) fileName = element.target.value.split('\\').pop();
      fileName ? $label.addClass('has-file').find('.js-fileName').html(fileName) : $label.removeClass('has-file').html(labelVal);
   });
  });

})();
</script>


@endsection

