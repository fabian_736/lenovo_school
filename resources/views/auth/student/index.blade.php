@extends('layouts.auth.app')
@section('content')


<body class="" style="background-image: url('img/background.png') !important;
background-size: 100% 100% !important;
background-repeat:no-repeat  !important;">


    <div class="row mx-auto row_login_index">
        <div class="col-12 px-5 col-principal_login_index" >
            <div class="card form_login_index" > 
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center align-items-center col_logo_login_index">
                        <img class="img-responsive logo_school_login_index" src="{{url('img/logo.png')}}" alt="" >
                    </div>
                    <div class="col d-flex justify-content-center align-items-center col_title_login_index">
                        <h3 for="" class="title_login_index fa-2x">Bienvenido</h3>
                    </div>
                    <form action="{{ route('student.auth') }}" method="POST" >
                        @csrf 
                        <div class="col-10 mx-auto">
                            <input type="text" name="email" class="form-control my-icon" placeholder="Usuario">
                        </div>
                        <div class="col-10 mx-auto my-md-4 my-sm-3 ">
                            <input type="password" name="password" class="form-control my-icon2" placeholder="Contraseña">
                        </div>
                        <div class="col-10 mx-auto d-flex justify-content-center">
                            <button type="submit" class="btn form-control text-white bg-success btn-submit_login_index d-flex align-items-center justify-content-center font-weight-bold">INGRESAR</button>
                        </div>
                        <div class="col-10 mx-auto d-flex justify-content-center">
                            <a href="{{route('student.date')}}" class="btn form-control text-success bg-white btn-create_login_index d-flex align-items-center justify-content-center font-weight-bold" >CREAR CUENTA</a>
                        </div>
                        <div class="col-10 mx-auto col-forgot_login_index">
                            <a href="{{route('student.firts_restore')}}" class="d-flex justify-content-center forgot_login_index" style="color: #4C7421">Olvidaste tu contraseña</a>
                        </div>
                    </form>

                </div>


            </div>
        </div>
    </div>

   
@endsection

