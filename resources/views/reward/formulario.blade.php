@extends('layouts.home.app')
@section('content')


<div class="row mr-0">
    <div class="col-1 d-flex justify-content-end align-items-center">
       <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <label for="" class="h3 title_form" ><span style="font-weight: bold">REDENCIÓN</span> PREMIO</label>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="" class="h5 subtitled_form">Para concluir la redención de tu premio llena estos campos:
                </label>
            </div>
        </div>
    </div>
</div>


<div class="row mr-0">
    <div class="col-3 card card-body mx-auto card_one">
        <div class="row-reverse">
            <div class="col d-flex justify-content-center  my-5 card_title">
                <label for="" class="h3 title_form" style="font-weight: bold">Card Spotify</label>
            </div>
            <div class="col d-flex justify-content-center">
                <img src="{{url('img/spotify.png')}}" alt="" class="img_form">
            </div>
        </div>
    </div>
    <div class="col-8 card card-body mx-auto px-5 card_two">

        <label for="" class="h3 my-5 title_form card_title_two" style="font-weight: bold; color: black">Formulario</label>

        <form action="" method="">
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Correo electrónico">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Teléfono">
            </div>
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Número de identificación">
            </div>

            <div class="row my-5 card_bottom">
                <div class="col d-flex justify-content-end">
                    <a href="javascript:;" class="btn text-white " data-toggle="modal" data-target="#exampleModalCenter" style="background: #7DBE38; border-radius: 20px; font-weight: bold ">
                    confirmar
                    </a>
                </div>
            </div>
           
        </form>
    </div>
</div>



<!-- Modal ¿ESTA SEGURO SALIR DE LA CLASE? -->
<div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <img src="{{url('svg/warning.svg')}}" alt="" class="w-25">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <h3 class="text-center" style="font-weight: bold">¡TE FALTAN PUNTOS!</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <label for="" class="lead text-center" style="color: black" >No podemos redimir tu premio, te faltan 50 puntos para reclamarlo</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <a href="{{route('reward.index')}}" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                        Buscar más premios
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>


<style>
    @media (max-width: 1366px){
        .title_form{
            font-size: 1.3em;
        }

        .subtitled_form{
            font-size: 1em;
        }

        .img_form{
            width: 75% !important;
        }

        .card_title{
            margin-top: 0 !important;
        }

        .card_title_two{
            margin-top: 3% !important;
        }

        .card_bottom{
            margin-bottom: 0 !important;
            margin-top: 2% !important;
        }

    }


    @media (max-width: 800px){
        .card_one{
            min-width: 100% !important;
        }

        .card_two{
            min-width: 100% !important;
        }

    }
</style>

@endsection