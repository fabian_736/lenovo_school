@extends('layouts.home.app')
@section('content')


<div class="row mr-0">
    <div class="col-1 d-flex justify-content-end align-items-center">
       <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <label for="" class="h3 titlerecompensa" style="font-weight: bold">RECOMPENSAS</label>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="" class="h5 subtitledrecompensa">Desbloquea las insignias y canjea premios con tus puntos por tu compromiso en Lenovo School.
                </label>
            </div>
        </div>
    </div>
</div>


<div class="row mx-auto bg-dark ">
    <div class="col card card-body my-0" >
        <div class="row mb-3">
            <div class="col-5 mx-auto">
                <a href="{{route('ranking.index')}}" >
                    <label for="" class="lead h3 titlerecompensa" style="color: #3F3E43; font-weight: bold; cursor: pointer">INSIGNIAS</label>
                </a>
            </div>
            <div class="col-5 mx-auto d-flex justify-content-end">
                <a href="{{route('premio.index')}}" >
                    <label for="" class="lead h3 titlerecompensa" style="text-decoration-line: underline; color: #4C7421; font-weight: bold; cursor: pointer">PREMIOS</label>
                </a>
            </div>
        </div>
        <div class="row style2" style="overflow: auto;  max-height: 50vh !important; min-height: 50vh !important" >
            <div class="col">
                <li class="row d-flex justify-content-center align-items-center mb-3">
                    <div class="col-5  mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('svg/insignias/master_lenovo.svg')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">MAESTRO LENOVO</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Conéctate de forma puntual a todas las sesiones en vivo y GANA un tablet Lenovo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-5 mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" >
                            <div class="row">
                                <div class="col-3 d-flex justify-content-center align-items-center">
                                    <img src="{{url('svg/inactive/inactive.svg')}}" alt="">
                                    <img src="{{url('svg/inactive/candado.svg')}}" alt="" style="position: absolute">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">RANKEO IMPECABLE</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Logra estar entre las primeras 20 posiciones del ranking y gana remios Spotify</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="row d-flex justify-content-center align-items-center mb-3">
                    <div class="col-5 mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" >
                            <div class="row">
                                <div class="col-3 d-flex justify-content-center align-items-center">
                                    <img src="{{url('svg/inactive/inactive.svg')}}" alt="">
                                    <img src="{{url('svg/inactive/candado.svg')}}" alt="" style="position: absolute">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">DISCIPLINA ABSOLUTA</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Completa 3 desafíos Lenovo correctamente y gena 500 puntos extra para </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-5 mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" >
                            <div class="row">
                                <div class="col-3 d-flex justify-content-center align-items-center">
                                    <img src="{{url('svg/inactive/inactive.svg')}}" alt="">
                                    <img src="{{url('svg/inactive/candado.svg')}}" alt="" style="position: absolute">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">DISCIPLINA ABSOLUTA</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Completa 3 desafíos Lenovo correctamente y gena 500 puntos extra para </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="row d-flex justify-content-center align-items-center mb-3">
                    <div class="col-5 mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('svg/insignias/master_lenovo.svg')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">MAESTRO LENOVO</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Conéctate de forma puntual a todas las sesiones en vivo y GANA un tablet Lenovo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-5 mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" >
                            <div class="row">
                                <div class="col-3 d-flex justify-content-center align-items-center">
                                    <img src="{{url('svg/inactive/inactive.svg')}}" alt="">
                                    <img src="{{url('svg/inactive/candado.svg')}}" alt="" style="position: absolute">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">RANKEO IMPECABLE</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Logra estar entre las primeras 20 posiciones del ranking y gana remios Spotify</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="row d-flex justify-content-center align-items-center mb-3">
                    <div class="col-5 mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('svg/insignias/master_lenovo.svg')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">MAESTRO LENOVO</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Conéctate de forma puntual a todas las sesiones en vivo y GANA un tablet Lenovo.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-5 mx-auto card card-body m-0 card_list" style="background: #c4beb652; border-radius: 20px">
                        <a href="javascript:;" >
                            <div class="row">
                                <div class="col-3 d-flex justify-content-center align-items-center">
                                    <img src="{{url('svg/inactive/inactive.svg')}}" alt="">
                                    <img src="{{url('svg/inactive/candado.svg')}}" alt="" style="position: absolute">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead titlerecompensa" style="color: #4C7421; font-weight:bold">RANKEO IMPECABLE</label>
                                        </div>
                                        <div class="col">
                                            <p class="h4 subtitledrecompensa">Logra estar entre las primeras 20 posiciones del ranking y gana remios Spotify</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
            </div>
        </div>
    </div>
</div>

<!-- Modal ¿ESTA SEGURO SALIR DE LA CLASE? -->
<div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <img src="{{url('svg/warning.svg')}}" alt="">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <h3 class="text-center" style="font-weight: bold">¡ATENCIÓN!</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <label for="" class="lead text-center" style="color: black; font-weight: bold" >Para ganar esta insignia deber terminar el modulo completo de Desafios ThinkShield</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <a href="javascript:;" data-dismiss="modal" aria-label="Close" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                        CONTINUAR
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>


<style>
    @media (max-width: 1366px){
        .titlerecompensa{
            font-size: 1.2em;
        }
        .subtitledrecompensa{
            font-size: 1em;
        }
    }

    @media (max-width: 800px){
        .card_list{
            min-width: 100% !important;
        }
    }

    
</style>

@endsection