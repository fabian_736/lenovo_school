
          <div class="counter" data-cp-percentage="85" data-cp-color="#3F3E43">
          </div>

<style>
  .counter{
    width: 50%;
  }

  .percentage{
    position: absolute;
    font-size: 3em;
    font-weight: bold;
  }
</style>
         

<script>
   document.addEventListener("DOMContentLoaded", function() {

var circleProgress = (function(selector) {
  var wrapper = document.querySelectorAll(selector);
  Array.prototype.forEach.call(wrapper, function(wrapper, i) {
    var wrapperWidth,
      wrapperHeight,
      percent,
      innerHTML,
      context,
      lineWidth,
      centerX,
      centerY,
      radius,
      newPercent,
      speed,
      from,
      to,
      duration,
      start,
      strokeStyle,
      text;

    var getValues = function() {
      wrapperWidth = parseInt(window.getComputedStyle(wrapper).width);
      wrapperHeight = wrapperWidth;
      percent = wrapper.getAttribute('data-cp-percentage');
      innerHTML = '<div class="row">'+
                      '<div class="col d-flex justify-content-center align-items-center">'+
                          '<span class="percentage"><strong>' + percent + '</strong> %</span>'+
                          '<canvas id="canvas" class="circleProgressCanvas" width="' + (wrapperWidth * 2) + '" height="' + wrapperHeight * 2 + '"></canvas>'+
                      '</div>'+
                    '</div>';
      wrapper.innerHTML = innerHTML;
      text = wrapper.querySelector(".percentage");
      canvas = wrapper.querySelector(".circleProgressCanvas");
      wrapper.style.height = canvas.style.width = canvas.style.height = wrapperWidth + "px";
      context = canvas.getContext('2d');
      centerX = canvas.width / 2;
      centerY = canvas.height / 2;
      newPercent = 0;
      speed = 1;
      from = 0;
      to = percent;
      duration = 1000;
      lineWidth = 50;
      radius = canvas.width / 2 - lineWidth;
      strokeStyle = wrapper.getAttribute('data-cp-color');
      start = new Date().getTime();
    };

    function animate() {
      requestAnimationFrame(animate);
      var time = new Date().getTime() - start;
      if (time <= duration) {
        var x = easeInOutQuart(time, from, to - from, duration);
        newPercent = x;
        text.innerHTML = Math.round(newPercent) + " %";
        drawArc();
      }
    }

    function drawArc() {
      var circleStart = 1.5 * Math.PI;
      var circleEnd = circleStart + (newPercent / 50) * Math.PI;
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.beginPath();
      context.arc(centerX, centerY, radius, circleStart, 4 * Math.PI, false);
      context.lineWidth = lineWidth;
      context.strokeStyle = "#E1E1E1";
      context.stroke();
      context.beginPath();
      context.arc(centerX, centerY, radius, circleStart, circleEnd, false);
      context.lineWidth = lineWidth;
      context.strokeStyle = strokeStyle;
      context.stroke();

    }
    var update = function() {
      getValues();
      animate();
    }
    update();

  });

  function easeInOutQuart(t, b, c, d) {
    if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
    return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
  }

});

circleProgress('.counter');

});

</script>