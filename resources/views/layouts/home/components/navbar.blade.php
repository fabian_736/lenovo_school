<nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
  <div class="container-fluid ">

  <div class="col-2 col-item-navbar col_1_navbar ">
      <a href="javascript:;" class="btn btn-lg btn-primary btn-navbar" style="border-radius: 40px">Puntos: 12.345</a>
  </div>
  <div class="col-6 col-item-navbar col_2_navbar">
      <a href="javascript:;" class="btn btn-lg btn-primary btn-navbar" style="border-radius: 40px">Avance Total: 12%</a>
  </div>

    <div class="toggler-navbar" style="display: none;">
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
    </div>

    <div class="col profile_portal">
        <div class="row">
            
              <div class="col d-flex justify-content-end align-items-center pr-0 profile_portal_text">
                  <div class="row-reverse">
                        <div class="col text-right">
                          <label for="" class="lead text-dark font-weight-bold">Javier Torres</label>
                        </div>
                        <div class="col text-right">
                            <label for="" class="text-primary">Vendedor</label>
                        </div>
                  </div>
              </div>
            
            <div class="col d-flex justify-content-start p-0 ">
                <div class="d-flex justify-content-center align-items-center " style="width:80px; height: 80px; border-radius: 60px; background: #8246AF">
                  <img src="{{url('img/user.png')}}" alt="" style="width:90%; height: 90%; border-radius: 60px;">
                  <a href="javascript:;" class="alert_profile d-flex justify-content-center align-items-center" role="button" data-html="true" data-toggle="popover" data-trigger="focus"
                  data-content="<div class='style2 container' style='max-height: 400px; overflow: auto'>


                    <!-- NOTIFICACIONES NUEVAS -->

                    <div class='row my-3'>
                        <div class='col'>
                            <label for='' style='color: #7DBE38'>Notificaciones nuevas</label>
                        </div>
                    </div>
                    <div class='row-reverse'>
                      <div class='col '>
                        <div class='row '>
                          <div class='col-2 p-0 d-flex justify-content-center align-items-center'>
                            <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                          </div>
                          <div class='col d-flex justify-content-center align-items-center mt-1'>
                            <label for='' class='text-dark' style='font-weight: bold;'>Hay un nuevo desafio, descubrelo.</label>
                          </div>
                        </div>
                      </div>
                      <div class='col p-0'>
                        <hr />
                      </div>
                      <div class='col '>
                        <div class='row '>
                          <div class='col-2 p-0 d-flex justify-content-center align-items-center'>
                            <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                          </div>
                          <div class='col d-flex justify-content-center align-items-center mt-1'>
                            <label for='' class='text-dark' style='font-weight: bold;'>Hay un nuevo desafio, descubrelo.</label>
                          </div>
                        </div>
                      </div>
                      <div class='col p-0'>
                        <hr />
                      </div>
                    </div>


                    <!-- NOTIFICACIONES ANTIGUAS -->

                    <div class='card p-3' style='background: #E5E5E5;'>
                      <div class='row my-3'>
                        <div class='col'>
                            <label for='' style='color: #8246AF'>Notificaciones antiguas</label>
                        </div>
                      </div>
                      <div class='row-reverse'>
                        <div class='col '>
                          <div class='row '>
                            <div class='col-2 p-0 d-flex justify-content-center align-items-center '>
                              <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                            </div>
                            <div class='col d-flex justify-content-center align-items-center mt-1'>
                              <label for='' class='font-weight-bold' >Hay un nuevo desafio, descubrelo.</label>
                            </div>
                          </div>
                        </div>
                        <div class='col p-0'>
                          <hr />
                        </div>
                        <div class='col '>
                          <div class='row '>
                            <div class='col-2 p-0 d-flex justify-content-center align-items-center'>
                              <img src='https://electronicssoftware.net/wp-content/uploads/user.png' alt='' class='w-100 rounded-circle'>
                            </div>
                            <div class='col d-flex justify-content-center align-items-center mt-1'>
                              <label for='' class='font-weight-bold'>Hay un nuevo desafio, descubrelo.</label>
                            </div>
                          </div>
                        </div>
                        <div class='col p-0'>
                          <hr />
                        </div>
                      </div>
                    </div>

                  </div>" id="example">
                  <div class="mt-5 mr-3 d-flex justify-content-center align-items-center" style="position: absolute; background: #46C8E1; width: 25px; height: 25px; border-radius: 40px">
                      <i class="material-icons text-white " style="font-size: 20px">notifications</i>
                  </div>
                  </a>
                </div>
            </div>
        </div>
    </div>
  </div>
</nav>

<script>
  function query() {
    const mediaQuery = window.matchMedia('(max-width: 900px)')

    if(mediaQuery.matches){
      $('.col_1_navbar').removeClass("col-2");
      $('.col_2_navbar').removeClass("col-6");
      $('.col_1_navbar').addClass("col");
      $('.col_2_navbar').addClass("col");
    }
  }

  window.onload = query;


</script>



<style>
 @media only screen and (max-width: 1366px) {
    .btn-navbar{
      font-size: 12px !important;
    }

    .col_1_navbar{
      margin-right: 5% !important;
    }

    .col_2_navbar{
      max-width: 40% !important;
    }

   
  }


  @media only screen and (max-width: 1280px) {
    .col-navbar-item{
      margin-right: 50px !important;
    }
  }

  @media only screen and (max-width: 991px) {
    .profile_portal{
      display: none;
    }

  }

  @media only screen and (max-width: 750px) {
    .profile_portal{
      display: none;
    }

    .btn-navbar{
      font-size: 10px !important;
      padding: 15% !important;
    }

    .toggler-navbar{
      display: block !important;
    }

    .col-item-navbar{
      width: 35%;
    }

  }
</style>
