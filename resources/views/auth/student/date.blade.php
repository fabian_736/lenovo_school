@extends('layouts.auth.app')
@section('content')


<body class="" style="background-image: url('img/background_date.png') !important;
background-size: 100% 100% !important;
background-repeat:no-repeat  !important;">


<div class="row mx-auto row_login_date">
    <div class="col-12 px-5 col-principal" >
        <div class="card form_login_date" style="overflow: auto"> 
            <div class="row-reverse">
                <div class="col d-flex justify-content-center align-items-center mt-5 col-logo_login_date">
                    <img src="{{url('img/logo.png')}}" alt="" class="img-responsive logo_date_login_date">
                </div>
                <div class="col d-flex justify-content-center align-items-center p-5 col-title_login_date">
                    <h3 for="" class="title-date_login_date" style="color: #7DBE38; font-weight: bold">Datos Adicionales</h3>
                </div>
                @include('errors.index')
                <form action="{{ route('student.register') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="col-10 mx-auto">
                        <input type="text" name="name" class="form-control inputform_login_date" placeholder="Nombres" value="{{ old('name') }}">
                    </div>
                    <div class="col-10 mx-auto">
                        <input type="text" name="lastname" class="form-control my-2 inputform_login_date" placeholder="Apellidos" value="{{ old('lastname') }}">
                    </div>
                    <div class="col-10 mx-auto">
                        <input type="text" name="cellphone_number" class="form-control inputform_login_date" placeholder="Télefono" value="{{ old('cellphone_number') }}">
                    </div>
                    <div class="col-10 mx-auto">
                        <input type="text" name="email" class="form-control my-2 inputform_login_date" placeholder="Correo electronico" value="{{ old('email') }}">
                    </div>
                    <div class="col-10 mx-auto">
                        <input type="text" name="position" class="form-control my-2 inputform_login_date" placeholder="Cargo" value="{{ old('position') }}">
                    </div>
                    <div class="col-10 mx-auto">
                        <input type="text" name="company" class="form-control inputform_login_date" placeholder="Empresa" value="{{ old('company') }}">
                    </div>
                    <div class="col-10 mx-auto">
                        <input type="text" name="channel" class="form-control my-2 inputform_login_date" placeholder="Canal" value="{{ old('channel') }}">
                    </div>
                    <div class="col-8 mt-5 mb-3 mx-auto col-bottom-file_login_date">
                        <div class="form-group">
                            <input type="file" name="file" id="file" class="input-file">
                            <label for="file" class="btn btn-tertiary js-labelFile_login_date d-flex align-items-center justify-content-center bg-white" style="border-radius: 40px;">
                                <img src="{{url('svg/camara.svg')}}" alt="" class="mr-2 icon-cam_login_date">
                                <span class="js-fileName_login_date text-success" style="font-size: 18px;">SUBIR FOTO</span>
                            </label>
                        </div>
                    </div>
                    <div class="col-8 mx-auto mb-5 col-submit_login_date">
                        <button type="submit" class="btn form-control text-white bg-success btn-create_login_date d-flex align-items-center justify-content-center font-weight-bold" style="border-radius: 40px; ">SIGUIENTE</button>
                    </div>
                    <!--<div class="col-8 mx-auto mb-5">
                        <a href="{{route('student.firts_restore')}}" class="btn form-control text-white" style="border-radius: 20px; background: #7DBE38; font-size: 20px;">SIGUIENTE</a>
                    </div>-->
                </form>
            </div>


        </div>
    </div>
</div>



      <script>
          (function() {

        'use strict';

        $('.input-file').each(function() {
          var $input = $(this),
              $label = $input.next('.js-labelFile_login_date'),
              labelVal = $label.html();

         $input.on('change', function(element) {
            var fileName = '';
            if (element.target.value) fileName = element.target.value.split('\\').pop();
            fileName ? $label.addClass('has-file').find('.js-fileName_login_date').html(fileName) : $label.removeClass('has-file').html(labelVal);
         });
        });

      })();
      </script>

@endsection

