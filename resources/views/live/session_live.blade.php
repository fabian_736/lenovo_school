@extends('layouts.home.app')
@section('content')

<div class="row pr-0 mr-0">
    <div class="col-8">
        <div class="row">
            <div class="col-1 d-flex align-items-center">
               <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter" ><i class="fas fa-arrow-circle-left text-success fa-2x"></i></a>
            </div>
            <div class="col pl-0">
                <div class="row-reverse">
                    <div class="col">
                        <label for="" class="h3 font-weight-bold title_sessionlive">INTRODUCCIÓN A LENOVO</label>
                    </div>
                    <div class="col">
                        <label for="" class="h5 subtitled_sessionlive">PORTAFOLIO DE ACCESORIOS</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col d-flex justify-content-end align-items-center pr-5">
        <label for="" class="h3 font-weight-bold title_sessionlive" style="color: #8246AF">DURACIÓN: 1h 30m</label>
    </div>
</div>

<div class="row pr-0 mr-0">
    <div class="col-8 col_vimeo"  >
        <div class="card m-0" style="height: 70%; width: 100%">
            <div style="padding:52% 0 0 0;position:relative;"><iframe src="https://vimeo.com/event/1930444/embed"  frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;"></iframe></div>            <div class="col my-3">
                <a href="#" onclick="like()" class="mr-3">
 <img src="{{url('svg/share/1.png')}}" alt="" class="animationIco"> <label for="" class="text-dark" style="font-size: 15px; font-weight: bold">Me gusta</label> 
                </a>
                <!-- 
                <a href="#">
                    <img src="{{url('svg/share/3.png')}}" alt="" class="animationIco"> <span class="mt-5 text-dark" >60</span> 
                </a>

            -->
            </div>
        </div>
    </div>
    <div class="col col_vimeo">
        <div class="card m-0 card_chatvimeo" style="height: 100%; width: 100%">
            <iframe  src="https://vimeo.com/event/1930444/chat/"   width="100%" height="100%" frameborder="0"></iframe>
        </div>
    </div>
</div>

<!-- Modal ¿ESTA SEGURO SALIR DE LA CLASE? -->
<div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <img src="{{url('svg/warning.svg')}}" alt="" class="icon_modal">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <label for="" class="lead text-center" style="color: black" >Aun no has terminado la Introducción a Lenovo</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <h3 class="text-center" style="font-weight: bold">¿Estas seguro de que quieres salir?</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5 col_btn_modal">
                    <a href="javascript:;" data-toggle="modal" data-target="#modal_two" data-dismiss="modal" class="btn text-white btn_modal" style="background: #8246AF; font-size: 16px;" >
                        Si, estoy seguro
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal HAS FINALIZADO LA CLASE -->
<div class="modal fade" id="modal_two" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <img src="{{url('svg/users.svg')}}" alt="" class="icon_modal">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <h3 class="text-center" style="font-weight: bold">¡Has finalizado la clase!</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <label for="" class="lead text-center" style="color: black" >Desafia tus nuevos conociemientos y prepárate para la siguiente clase.</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <a href="{{route('desafios.index')}}" class="btn text-white btn_modal" style="background: #8246AF; font-size: 16px;" >
                        cumplir el desafio
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <a href="{{route('class_save.index')}}" class="btn text-dark btn_modal" style="border-style: solid; border-width: 2px; border-color: #8246AF; font-size: 16px; background: white;" >
                        Ver clases grabadas
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>


<style>

    .animationIco{
        transition: transform .2s;
    }

    .animationIco:hover{
        transform: scale(1.5);
    }

    .backgroundModal{
        background-image: url('/img/home.png');
        background-attachment: fixed;
        background-size: cover;
        background-repeat: no-repeat;
    }

    @media (max-width: 1366px){
        .title_sessionlive{
            font-size: 1.5em;
        }

        .subtitled_sessionlive{
            font-size: 1em;
        }

        .icon_modal{
            width: 25% !important;
        }

        .btn_modal{
            font-size: 14px !important;
        }

        .col_btn_modal{
            margin-bottom: 1% !important;
        }

        
    }

    @media (max-width: 900px){
        .col_vimeo{
            min-width: 100% !important;
            margin-bottom: 25%
        }
        .card_chatvimeo{
            height: 500px !important;
            width: 100%;
        }
    }
</style>

<script>
    const like = () => {
        Swal.fire(
            '¡Buen Trabajo!',
            'Has dado like a tu primera clase en vivo',
            'success'
        )
    }

    const share = () => {
        Swal.fire({
            title: '<strong>Compartir en:</u></strong>',
            icon: 'info',
            html:
                '<div class="row p-0 m-0">'+
                    '<div class="col">'+
                            '<a href="#"><i class="fab fa-facebook fa-2x"></i></a>'+
                            '<a href="#"><i class="fab fa-twitter fa-2x mx-5 "></i></a>'+
                            '<a href="#"><i class="fab fa-linkedin fa-2x"></i></a>'+
                    '</div>'+
                '</div>',
            showCloseButton: false,
            showCancelButton: false,
            focusConfirm: false,
            
            })
        }

</script>
@endsection
