<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="{{url('svg/icon_lenovo.svg')}}">
  <link rel="icon" type="image/png" href="{{url('svg/icon_lenovo.svg')}}">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Lenovo School
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link href="http://fonts.cdnfonts.com/css/gotham" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.1/main.min.css">
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
  <link href="{{url('css/app.css')}}" rel="stylesheet" />
  <link href="{{url('css/auth.css')}}" rel="stylesheet" />
</head>



    @yield('content')


    <script src="{{url('js/app.js')}}"></script>


</body>
</html>




<style>

    * {
        font-family: Gotham !important;
        font-style: normal !important;
    }


.form{
    background-color: rgba(255, 255, 255, 0.808);
    border-radius: 20px;

}


.my-icon {
    padding-right: calc(1.5em + .75rem);
    background-image: url('{{url('svg/mail.svg') }}');
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc(.75em + .375rem) calc(.75em + .375rem);
}

.my-icon2 {
    padding-right: calc(1.5em + .75rem);
    background-image: url('{{url('svg/candado.svg') }}');
    background-repeat: no-repeat;
    background-position: center right calc(.375em + .1875rem);
    background-size: calc(.75em + .375rem) calc(.75em + .375rem);
}


input{
    border: 2px solid #000000 !important;
box-sizing: border-box !important;
border-radius: 10px !important;
    font-family: 'Gotham', sans-serif !important;
    background-color: rgba(223, 223, 223, 0.808) !important;
    padding: 20px !important;
}


</style>

<script>
    $( "#1" ).click(function() {
        $("#point_2").css({
            'background' : 'white'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/1.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'block'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#2" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/2.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'block'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#3" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : '#7DBE38'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/3.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'block'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#4" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : '#7DBE38'
        });
        $("#point_4").css({
            'background' : '#7DBE38'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/4.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'block'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#5" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : '#7DBE38'
        });
        $("#point_4").css({
            'background' : '#7DBE38'
        });
        $("#point_5").css({
            'background' : '#7DBE38'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/5.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'block'
        });
    });


    
</script>

<script>
 function deshabilitaRetroceso(){
    window.location.hash="no-back-button";
    window.location.hash="Again-No-back-button" //chrome
    window.onhashchange=function(){window.location.hash="";}
}
    </script>


<script>
     $( "#point_1" ).click(function() {
        $("#point_2").css({
            'background' : 'white'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/1.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'block'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#point_2" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : 'white'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/2.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'block'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#point_3" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : '#7DBE38'
        });
        $("#point_4").css({
            'background' : 'white'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/3.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'block'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#point_4" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : '#7DBE38'
        });
        $("#point_4").css({
            'background' : '#7DBE38'
        });
        $("#point_5").css({
            'background' : 'white'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/4.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'block'
        });

        $("#6").css({
            'display': 'none'
        });
    });

    $( "#point_5" ).click(function() {
        $("#point_2").css({
            'background' : '#7DBE38'
        });
        $("#point_3").css({
            'background' : '#7DBE38'
        });
        $("#point_4").css({
            'background' : '#7DBE38'
        });
        $("#point_5").css({
            'background' : '#7DBE38'
        });
        $(".body").css({
            'background-image': 'url("img/tutorial/5.png")',
            'background-size': '100% 100%',
            'background-repeat':'no-repeat',
            'background-attachment': 'fixed'
        });

        $("#1").css({
            'display': 'none'
        });

        $("#2").css({
            'display': 'none'
        });

        $("#3").css({
            'display': 'none'
        });

        $("#4").css({
            'display': 'none'
        });

        $("#5").css({
            'display': 'none'
        });

        $("#6").css({
            'display': 'block'
        });
    });
</script>