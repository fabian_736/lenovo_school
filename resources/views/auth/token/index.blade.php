@extends('layouts.auth.app')
@section('content')

<body class="body">
    
   <div class="row-reverse">
       <div class="col mt-5">
            <img src="{{url('img/logo_blanco.png')}}" alt="">
       </div>
       <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 my-5 cardtoken">
            <div class="card">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center mt-5">
                        <label for="" class="h1" style="color: #3F3E43">Bienvenido</label>
                    </div>
                    <div class="col d-flex justify-content-center my-3">
                        <label for="" class="lead h2 mx-5 text-center">Estamos verificando tus credenciales, <br> por favor espera un momento</label>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <img src="{{url('svg/icon_lenovo.svg')}}" alt="" class="icon_lenovo">
                    </div>
                    <div class="col d-flex justify-content-center">
                        @include('layouts.home.utils.progress.auth')
                    </div>
                </div>
            </div>
       </div>
   </div>

    <style>
        .body{
            background-image: url('/img/background.png');
            background-attachment: fixed;
            background-size: cover;
            background-repeat: no-repeat;
        }

        .card{
            border-radius: 20px;
        }

       @media (min-width: 768px) and (max-width: 1300px){
        .cardtoken{
            min-width: 100%;
        }
       }

       @media (max-width: 1366px){
        .icon_lenovo{
            max-width: 50%;
        }
    }
    </style>


@endsection