@extends('layouts.home.app')
@section('content')

<div class="row p-0 mr-0">
    <div class="col-4 col_firts">
        <div class="row-reverse">
            <div class="col-12" >
                <div class="card" style="width: 100%; max-height: 100%; border-radius: 20px;">
                @include('layouts.home.utils.calendar.index')
                </div>
            </div>
            <div class="col">
                <li class="list_1 fa-md">Clase actual</li>
                <li class="list_2 fa-md">Próximas clases</li>
                <li class="list_3 fa-md">Clases anteriores</li>
            </div>
            <style>
                    .list_1 {
                        list-style-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' style='fill: green' viewBox='-2.2 -2.4 4 4'><circle r='1' /></svg>");
                    }

                    .list_2 {
                        list-style-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' style='fill: red' viewBox='-2.2 -2.4 4 4'><circle r='1' /></svg>");
                    }

                    .list_3 {
                        list-style-image: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' width='25' height='25' style='fill: secondary' viewBox='-2.2 -2.4 4 4'><circle r='1' /></svg>");
                    }
            </style>
        </div>
    </div>
    <div class="col col_second">
        <div class="row">
            <div class="col" >
                <div class="card card_second" >
                    <div class="row mx-auto pb-0">
                        <div class="col d-flex justify-content-center align-items-center pr-0 col_img_live">
                            <img src="{{url('img/live.png')}}" alt="" class="w-100">
                        </div>
                        <div class="col p-0 col_description">
                            <div class="row-reverse">
                                <div class="col">
                                    <h3 class="font-weight-bold title_live">INTRODUCCIÓN A LENOVO</h3>
                                    <h5 class="subtitled_live">PORTAFOLIO DE ACCESORIOS</h5>
                                </div>
                                <div class="col">
                                    <h5 class="text_description">16  ·  NOV  ·  21   |   02:30 PM
                                        DURACIÓN: 1h 30m</h5>
                                </div>
                                <div class="col">
                                    <h5 class="text_description">Expositor: Javier Ortíz</h5>
                                </div>
                                <div class="col">
                                    <h5 class="text_description">Descripción: En este encuentro profundizaremos en las ventajas y variedad de nuestro portafolio de accesorios.</h5>
                                </div>
                                <div class="col">
                                    <a href="{{route('session_live.index')}}" class="btn w-100" style="background: #7DBE38">ENTRAR</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row ">
                        <div class="col ">
                            <a href="{{route('class_save.index')}}" class="btn w-100 font-weight-bold mb-0" style="background: #C4BEB6">IR A CLASES GRABADAS</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
   
</div>

<style>

    @media (max-width: 1920px) and (min-width: 1367px){
        .title_live{
            font-size: 2em !important;
        }

        .subtitled_live{
            font-size:1.5em !important;
        }

        .text_description{
            font-size:1.2em !important;
        }

        .list_1{
            font-size: 1.5em !important;
            margin-bottom: 5px;
        }

        .list_2{
            font-size: 1.5em !important;
            margin-bottom: 5px;
        }

        .list_3{
            font-size: 1.5em !important;
            margin-bottom: 5px;
        }

    }

    @media (min-width: 1366px){
        .title_live{
            font-size: 1.2em;
        }

        .subtitled_live{
            font-size:1em;
        }

        .card_second{
            max-height: 100% !important;
            min-width: 100% !important;
            max-width: 100% !important;
        }

        .text_description{
            font-size: 0.8em;
        }
    }
    

    @media (max-width: 1365px) and (min-width: 1200px){
      
        .title_live{
            font-size: 1.2em;
        }

        .subtitled_live{
            font-size:1em;
        }

        .text_description{
            font-size: 0.8em;
        }
    }   
    
    @media (max-width: 1199px){
        .col_firts{
            min-width: 100% !important;
        }

        .col_second{
            min-width: 100% !important;
        }
    }


    @media (max-width: 600px){
        .col_img_live{
            min-width: 100% !important;
            margin-top: 5%;
            padding-right: 2% !important;
        }

        .col_description{
            min-width: 100% !important;
        }
    }
</style>


@endsection
