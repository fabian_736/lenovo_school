@extends('layouts.home.app')
@section('content')

<div  style="position: absolute; z-index: 100000;" class="class_list_save">

    <div class="h-100" style="position: fixed; top: 0; bottom: 0; right: 0; width: 20%; background-image: url('/img/background_sidebar_ticket.png'); 
    background-size: 100% 100%;
    background-repeat: no-repeat;">
    
    <div class="row-reverse">
        <div class="col d-flex justify-content-center">
            <label for="" class="text-white lead" style="font-weight: bold; margin-top: 40%; margin-bottom: 20%">CLASES ANTERIORES</label>
        </div>
        <div class="col" style="overflow: auto; height: 50vh !important;">
          <ul>
              <li class="row mb-3">
                 <div class="col-3 p-0" >
                        <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                  
                 </div>
                 <div class="col">
                     <div class="row-reverse">
                         <div class="col">
                             <span class="text-white">CLASE 1</span>
                         </div>
                         <div class="col">
                             <span class="text-white">Lorem ipsum dolor sit amet</span>
                         </div>
                     </div>
                 </div>
              </li>
              <li class="row mb-3">
                <div class="col-3 p-0" >
                       <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                 
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col">
                            <span class="text-white">CLASE 1</span>
                        </div>
                        <div class="col">
                            <span class="text-white">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                </div>
             </li>
             <li class="row mb-3">
                <div class="col-3 p-0" >
                       <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                 
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col">
                            <span class="text-white">CLASE 1</span>
                        </div>
                        <div class="col">
                            <span class="text-white">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                </div>
             </li>
             <li class="row mb-3">
                <div class="col-3 p-0" >
                       <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                 
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col">
                            <span class="text-white">CLASE 1</span>
                        </div>
                        <div class="col">
                            <span class="text-white">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                </div>
             </li>
             <li class="row mb-3">
                <div class="col-3 p-0" >
                       <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                 
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col">
                            <span class="text-white">CLASE 1</span>
                        </div>
                        <div class="col">
                            <span class="text-white">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                </div>
             </li>
             <li class="row mb-3">
                <div class="col-3 p-0" >
                       <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                 
                </div>
                <div class="col">
                    <div class="row-reverse">
                        <div class="col">
                            <span class="text-white">CLASE 1</span>
                        </div>
                        <div class="col">
                            <span class="text-white">Lorem ipsum dolor sit amet</span>
                        </div>
                    </div>
                </div>
             </li>
          </ul>
        </div>
    </div>

    </div>
    
        </div>

    


<div class="row pr-0 mr-0">
    <div class="col-8">
        <div class="row">
            <div class="col-1 d-flex align-items-center">
               <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
            </div>
            <div class="col pl-0">
                <div class="row-reverse">
                    <div class="col">
                        <label for="" class="h3 font-weight-bold title_class_save">CLASES GRABADAS</label>
                    </div>
                    <div class="col">
                        <label for="" class="h5 subtitled_class_save">Consulta las clases grabadas para reforzar tus conocimientos de Lenovo School.</label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row pr-0 mr-0">
    <div class="col-3 col_firts" >
        <div class="card card-body shadow col_card_save" style="overflow: auto; border-radius: 20px">
            <div class="row-reverse">
                <div class="col">
                    <h3 class="titleIntroduction" style="font-weight: bold">INTRODUCCIÓN A LENOVO</h3>
                </div>
                <div class="col ">
                   <p class="subtitledIntroduction">PORTAFOLIO DE ACCESORIOS</p> 
                </div>
                <div class="col">
                    <p> <b class="subtitledIntroduction" style="font-weight: bold">Duración:</b> 1h 30m </p>
                </div>
                <div class="col">
                    <hr>
                </div>
                <div class="col">
                    <p> <b class="subtitledIntroduction" style="font-weight: bold">Expositor: Javier Ortíz</b> </p>
                </div>
                <div class="col">
                    <p> <b class="subtitledIntroduction" style="font-weight: bold">Descripción:</b> En este encuentro profundizaremos en las ventajas y variedad de nuestro portafolio de accesorios. </p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-7 col_second">
        <div class="col card card-body col_card_video" style="border-radius: 20px;">
            <video  controls style="border-radius: 20px; height: 50vh; object-fit:fill;" poster="{{url('img/live.png')}}">
                <source src="https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4" type="video/mp4">
                Your browser does not support the video tag.
            </video>
        </div>
    </div>

    <div class="col col_thrid" style="display: none; background-image: url('/img/background_sidebar.png'); 
    background-size: cover;
    background-repeat: no-repeat;">
        <div class="row-reverse">
            <div class="col d-flex justify-content-center">
                <label for="" class="text-white lead title_list_save" style="font-weight: bold; margin-top: 40%; margin-bottom: 20%">CLASES ANTERIORES</label>
            </div>
            <div class="col col_list_save_plus" style="overflow: auto; height: 50vh !important;">
              <ul>
                  <li class="row mb-3">
                     <div class="col-3 p-0" >
                            <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                      
                     </div>
                     <div class="col">
                         <div class="row-reverse">
                             <div class="col">
                                 <span class="text-white">CLASE 1</span>
                             </div>
                             <div class="col">
                                 <span class="text-white">Lorem ipsum dolor sit amet</span>
                             </div>
                         </div>
                     </div>
                  </li>
                  <li class="row mb-3">
                    <div class="col-3 p-0" >
                           <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                     
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <span class="text-white">CLASE 1</span>
                            </div>
                            <div class="col">
                                <span class="text-white">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                 </li>
                 <li class="row mb-3">
                    <div class="col-3 p-0" >
                           <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                     
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <span class="text-white">CLASE 1</span>
                            </div>
                            <div class="col">
                                <span class="text-white">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                 </li>
                 <li class="row mb-3">
                    <div class="col-3 p-0" >
                           <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                     
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <span class="text-white">CLASE 1</span>
                            </div>
                            <div class="col">
                                <span class="text-white">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                 </li>
                 <li class="row mb-3">
                    <div class="col-3 p-0" >
                           <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                     
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <span class="text-white">CLASE 1</span>
                            </div>
                            <div class="col">
                                <span class="text-white">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                 </li>
                 <li class="row mb-3">
                    <div class="col-3 p-0" >
                           <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                     
                    </div>
                    <div class="col">
                        <div class="row-reverse">
                            <div class="col">
                                <span class="text-white">CLASE 1</span>
                            </div>
                            <div class="col">
                                <span class="text-white">Lorem ipsum dolor sit amet</span>
                            </div>
                        </div>
                    </div>
                 </li>
              </ul>
            </div>
        </div>
    </div>
</div>

<style>
         
    @media (min-width: 1920px){
        .titleIntroduction{
            font-size: 1.5625rem !important;
        }

        .cardlist{
            height: 600px !important;
        }
    }

    @media (max-width: 1366px){
        .title_class_save{
            font-size: 1.5em !important;
        }

        .subtitled_class_save{
            font-size: 1em !important;
        }

        .titleIntroduction{
            font-size: 1em !important;
        }

        .subtitledIntroduction{
            font-size: 0.8em !important;
        }

        .col_card_save{
            max-height: 65% !important;
        }

        .col_card_video{
            max-height: 65% !important;
        }

        .cardlist{
            height: 400px !important;
        }
    }

    @media (max-width: 1220px) and (min-width: 1150px){
        .col_card_save{
            max-height: 40% !important;
        }
    }

    @media (max-width: 1149px){
        .class_list_save{
            display: none;
        }

        .col_firts{
            min-width: 100% !important;
            min-height: auto !important;                                                                                     
        }

        .col_second{
            min-width: 100% !important;
            min-height: auto !important;                                                                                         
        }

        .col_thrid{
            min-width: 100% !important;
            min-height: auto !important; 
            display: block !important;
        }

        .title_list_save{
            margin-top: 5% !important;
            margin-bottom: 5% !important;
        }

        .col_list_save_plus{
            max-width: 70% !important;
            margin: auto !important;
        }

    }
</style>


@if(request()->is('live/class_save'))

<style>
    .col_1_navbar{
        max-width: 20% !important;
    }

    .col_2_navbar{
        max-width: 25% !important;
    }

    .profile_portal_text{
        display: flex;
        justify-content: end !important;
        max-width: 30% !important;
    }

    @media (max-width: 1149px){
            .col_1_navbar{
                max-width: 30% !important;
        }

        .col_2_navbar{
            max-width: 30% !important;
        }

        .profile_portal_text{
        max-width: 40% !important;
        }
    }

</style>

@endif

@endsection