<div class="sidebar" data-image="{{url('img/background_sidebar.png')}}" style="z-index: 100000;">

  <div class="navbar-wrapper mt-5" style="position: absolute; z-index: 100000; right: -20px;">
      <div class="navbar-minimize">
        <button id="minimizeSidebar" class="btn btn-just-icon btn-fab btn-round" style="background: #7DBE38">
          <i  onclick="arrowleft_sidebar()" class="material-icons text_align-center visible-on-sidebar-regular">arrow_left</i>
          <i  onclick="arrowright_sidebar()" class="material-icons design_bullet-list-67 visible-on-sidebar-mini">arrow_right</i>
        </button>
       
      </div>
  </div>
  <div class="logo d-flex justify-content-center" style="padding-top: 30%; padding-bottom: 50%">
      <img src="{{url('img/logo_blanco.png')}}" style="max-width: 60%" class="logo_lenovo"/>
      <img src="{{url('svg/icon_lenovo_blanco.svg')}}" style="max-width: 60%; display: none" class="icon_lenovo"  />
  </div>
  
<script>
  function arrowleft_sidebar(){
    $(".icon_lenovo").show();
    $(".logo_lenovo").hide();
    $('.w-50').removeClass("w-50");
    $('.img_home_sidebar').removeClass("img_home_sidebar_mini");
    $('.img_home_sidebar').addClass("ml-5");
    $('.logo').css("margin-top", "100%");
  }

  function arrowright_sidebar(){
    $(".logo_lenovo").show();
    $(".icon_lenovo").hide();
    $('.img_home_sidebar').addClass("img_home_sidebar_mini");
    $('.img_home_sidebar').addClass("w-50");
    $('.img_home_sidebar').removeClass("ml-5");
    $('.logo').css("margin-top", "0%");
  }
</script>

  <div class="sidebar-wrapper">
      <ul class="nav">
        <li class="nav-item {{ request()->is('home') ? 'active' : '' }} ">
          <a class="nav-link" href="{{route('portal.index')}}">
            <div class="row">
              <div class="d-flex justify-content-center" style="width: 25%">
                @if(request()->is('home'))
               <style>
                 .img_home{
                   display: none;
                 }
               </style>
                <img src="{{url('svg/home_active.svg')}}" alt="" class="img_home_sidebar img_home_sidebar_mini w-50">
                @endif
                <img src="{{url('svg/home.svg')}}" alt=""  class="img_home w-50 img_home_sidebar img_home_sidebar_mini">
              </div>
              <div class="w-75 d-flex align-items-center"><p> INICIO ㅤㅤㅤㅤㅤㅤㅤ</p></div>
            </div>
          </a>
        </li>
        
        <li class="nav-item {{ request()->is('live', 'live/session_live', 'live/class_save') ? 'active' : '' }}">
          <a class="nav-link" href="{{route('live.index')}}">
            <div class="row">
              <div class="d-flex justify-content-center" style="width: 25%">
                @if(request()->is('live', 'live/session_live', 'live/class_save'))
                <style>
                  .img_class{
                    display: none;
                  }
                </style>
                 <img src="{{url('svg/class_active.svg')}}" alt="" class="img_home_sidebar w-50 img_home_sidebar_mini">
                 @endif
                <img src="{{url('svg/class.svg')}}" alt="" class="w-50 img_home_sidebar img_class img_home_sidebar_mini" >
              </div>
              <div class="w-75  d-flex align-items-center"> <p> CLASES EN VIVO ㅤㅤ</p></div>
            </div>
          </a>
        </li>
        <li class="nav-item {{ request()->is('desafios', 'desafios/mis_desafios', 'desafios/prueba') ? 'active' : '' }}">
          <a class="nav-link" href="{{route('desafios.index')}}">
            <div class="row">
              <div class="w-25 d-flex justify-content-center ">
                @if(request()->is('desafios', 'desafios/mis_desafios', 'desafios/prueba'))
                <style>
                  .img_desafios{
                    display: none;
                  }
                </style>
                 <img src="{{url('svg/desafios_active.svg')}}" alt="" class="img_home_sidebar w-50 img_home_sidebar_mini ">
                 @endif
                <img src="{{url('svg/desafios.svg')}}" alt=""  class=" w-50 img_home_sidebar img_desafios img_home_sidebar_mini">
              </div>
              <div class="w-75  d-flex align-items-center"> <p> DESAFIOS ㅤㅤㅤㅤㅤㅤ</p></div>
            </div>
          </a>
        </li>
        <li class="nav-item {{ request()->is('galeria', 'galeria/video') ? 'active' : '' }}">
          <a class="nav-link" href="{{route('galeria.index')}}">
            <div class="row">
              <div class="w-25 d-flex justify-content-center ">
                @if(request()->is('galeria', 'galeria/video'))
                <style>
                  .img_galeria{
                    display: none;
                  }
                </style>
                 <img src="{{url('svg/galeria_active.svg')}}" alt="" class="img_home_sidebar w-50 img_home_sidebar_mini">
                 @endif
                <img src="{{url('svg/galeria.svg')}}" alt="" class="w-50 img_home_sidebar img_galeria img_home_sidebar_mini">
              </div>
              <div class="w-75  d-flex align-items-center"> <p> GALERIA ㅤㅤㅤㅤㅤㅤㅤ</p></div>
            </div>
          </a>
        </li>
        <li class="nav-item {{ request()->is('perfil') ? 'active' : '' }}">
          <a class="nav-link" href="{{route('perfil.index')}}">
            <div class="row">
              <div class="w-25 d-flex justify-content-center">
                @if(request()->is('perfil'))
                <style>
                  .img_perfil{
                    display: none;
                  }
                </style>
                 <img src="{{url('svg/user_active.svg')}}" alt="" class="img_home_sidebar w-50 img_home_sidebar_mini">
                 @endif
                <img src="{{url('svg/user.svg')}}" alt="" class="w-50 img_home_sidebar img_perfil img_home_sidebar_mini">
              </div>
              <div class="w-75  d-flex align-items-center"> <p> PERFIL ㅤㅤㅤㅤㅤㅤㅤ</p></div>
            </div>
          </a>
        </li>
        <li class="nav-item {{ request()->is('ranking') ? 'active' : '' }}">
          <a class="nav-link" href="{{route('ranking.index')}}">
            <div class="row">
              <div class="w-25 d-flex justify-content-center" >
                @if(request()->is('ranking'))
                <style>
                  .img_ranking{
                    display: none;
                  }
                </style>
                 <img src="{{url('svg/ranking_active.svg')}}" alt="" class="img_home_sidebar w-50 img_home_sidebar_mini">
                 @endif
                <img src="{{url('svg/ranking.svg')}}" alt="" class="w-50 img_home_sidebar img_ranking img_home_sidebar_mini">
              </div>
              <div class="w-75  d-flex align-items-center"> <p> RANKING ㅤㅤㅤㅤㅤㅤ</p></div>
            </div>
          </a>
        </li>
        <li class="nav-item {{ request()->is('reward', 'reward/premio','reward/formulario') ? 'active' : '' }}">
          <a class="nav-link" href="{{route('reward.index')}}">
            <div class="row">
              <div class="w-25 d-flex justify-content-center">
                @if(request()->is('reward', 'reward/premio','reward/formulario'))
                <style>
                  .img_reward{
                    display: none;
                  }
                </style>
                 <img src="{{url('svg/recompensas_active.svg')}}" alt="" class="img_home_sidebar w-50 img_home_sidebar_mini ">
                 @endif
                <img src="{{url('svg/recompensas.svg')}}" alt="" class="w-50 img_home_sidebar img_reward img_home_sidebar_mini">
              </div>
              <div class="w-75  d-flex align-items-center"> <p> RECOMPENSAS ㅤㅤㅤ</p></div>
            </div>
          </a>
        </li>
        <li class="nav-item logout_sidebar" style="margin-top: 50%">
          <a class="nav-link" href="#">
            <form action="{{route('logout')}}" method="POST" id="logout">
              @csrf
            <div class="row">
              <div class="w-25 d-flex justify-content-center">
                <img src="{{url('svg/logout.svg')}}" alt="" class="w-50 img_home_sidebar img_home_sidebar_mini">
              </div>
              <div class="w-75 d-flex align-items-center " onclick="$('#logout').submit()"> <p> CERRAR SESIÓN</p></div>
            </div>
          </form>
          </a>
        </li>
      </ul>
  </div>
</div>


<style>
  @media only screen and (max-width: 1366px) {

    .logo{
      padding-bottom: 20% !important;
    }

    .sidebar .nav p {
      line-height: 15px !important;
      font-size: 12px;
    }

    .logout_sidebar{
      margin-top: 20% !important;
    }

    .img_home_sidebar_mini{
      max-width: 30% !important;
    }


  }
</style>

