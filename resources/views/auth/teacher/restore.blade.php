@extends('layouts.auth.app')
@section('content')

<body style=" background-image: url('/img/background_t.png');
background-repeat: no-repeat;
background-position: center;
background-size: cover;
width: 100%;
height:100vh;
padding: 5%;">

<div class="row">
    <div class="col-md-4 col-lg-4 col-sm-10 col-xs-10">
        <div class="card form p-5">
            <div class="row-reverse">
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <img src="{{url('img/logo.png')}}" alt="">
                </div>
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <h3 for="" style="color: #7DBE38; font-family: 'Gotham', sans-serif;">Reestablece tu contraseña</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center mb-3">
                    <label for="" class="text-center" style="font-size: 20px; font-family: 'Gotham', sans-serif;">Ingresa una combinación de 8 carácteres mínimo</label>
                </div>
                <form action="">
                    <div class="col">
                        <input type="text" id="date" class="form-control my-icon" placeholder="Nueva contraseña">
                    </div>
                    <div class="col my-5">
                        <input type="text" id="date" class="form-control my-icon2" placeholder="Nueva contraseña">
                    </div>
                    <div class="col-8 my-5 mx-auto">
                        <a href="{{route('teacher.date')}}" class="btn form-control text-white d-flex justify-content-center align-items-center" style="border-radius: 20px; background: #7DBE38; font-size: 20px;">SIGUIENTE <i class="fas fa-caret-right fa-lg ml-2"></i></a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection

