  <!-- Modal HAS FINALIZADO LA PRUEBA -->
  <div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <img src="{{url('svg/users.svg')}}" alt="" class="icon_modal">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <h3 class="text-center" style="font-weight: bold">¡FELICIDADES!</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <label for="" class="lead text-center" style="color: black" >Has completado el Desafio ThinkShield correctamente</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <a href="javascript:;" data-toggle="modal" data-target="#modal_2" data-dismiss="modal" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                        SALIR
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>

    <!-- Modal NUEVO INTENTO -->
    <div class="modal fade" id="modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog " role="document">
          <div class="modal-content backgroundModal">
            <div class="modal-body ">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-end align-items-end">
                        <a href="javascript:;">
                            <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                        </a>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center my-5">
                        <img src="{{url('svg/error.svg')}}" alt="" class="icon_modal">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <h3 class="text-center" style="font-weight: bold">¡Ups, fallaste!</h3>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <label for="" class="lead text-center" style="color: black" >
                            <b style="color: #8246AF; font-weight: bold">¿Quieres volverlo a intentar y ganar 300 puntos?</b> <br>
                            Mira este video <b style="font-weight: bold">LENOVO</b> y obtén otro intento</label>
                    </div>
                    <div class="row">
                        <div class="col d-flex justify-content-center align-items-center mt-5">
                            <a href="javascript:;" onclick="item_display_1()" data-toggle="modal" data-target="#modal_3" data-dismiss="modal" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                                Si
                            </a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mt-5">
                            <a href="{{route('portal.index')}}" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                                No
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
          </div>
        </div>
      </div>


          <!-- Modal VIDEO NUEVO INTENTO -->
    <div class="modal fade" id="modal_3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content backgroundModal">
            <div class="modal-body ">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-end align-items-end">
                        <a href="javascript:;">
                            <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                        </a>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center my-3 col_svg">
                        <img src="{{url('svg/error.svg')}}" alt="" class="icon_modal_2">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center">
                        <label for="" class="lead text-center" style="color: black" >
                           ¡Gana tu nuevo intentoviendo este video <br>
                           y aprende más de <b style="color: #8246AF; font-weight: bold">Lenovo ThinkShield!</b></label>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center my-5 col_video">
                            <video  controls style="border-radius: 20px;">
                                <source src="https://www.learningcontainer.com/wp-content/uploads/2020/05/sample-mp4-file.mp4" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        
                    </div>
                    <div class="row">
                        <div class="col d-flex justify-content-center align-items-center mt-5 ">
                            <a href="{{route('desafios.index')}}" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                                DESBLOQUEAR NUEVO INTENTO
                            </a>
                        </div>
                    </div>
                    
                </div>
            </div>
          </div>
        </div>
      </div>



      <style>
           .backgroundModal{
        background-image: url('/img/home.png');
        background-attachment: fixed;
        background-size: cover;
        background-repeat: no-repeat;
    }

    @media (max-width: 1366px){
        .icon_modal{
            width: 25% !important;
        }

        .icon_modal_2{
            max-width: 100px !important;
        }

        .col_video{
            margin-bottom: 0 !important;
        }

        .col_svg{
            margin-top: 0 !important;
        }
    }
      </style>