@extends('layouts.home.app')
@section('content')


        <div class="row pr-0 mr-0">
            <div class="col-1 d-flex justify-content-end align-items-center">
               <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col">
                        <label for="" class="h3 titleback_desafios"><span style="font-weight: bold">DESAFIOS</span> | LENOVO</label>
                    </div>
                    <div class="col d-flex justify-content-end align-items-end">
                        <a href="{{route('desafios.mis_desafios')}}">
                            <label for="" class="h3 titleback_desafios" style="color: #4C7421; font-weight: bold; text-decoration-line: underline; cursor: pointer">MIS DESAFIOS</label>
                        </a>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label for="" class="h5 subtitledback_desafios">Desafía tus conocimientos, prueba qué tanto sabes de Lenovo y gana puntos para canjear premios y recompensas por tus logros.</label>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mx-auto d-flex justify-content-between row_firtscard">
            <div class="col col_desafios">
                <div class="backgroundDesafios d-flex justify-content-center align-items-end pb-3">
                    <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                        <div class="row">
                            <div class="col d-flex align-items-center justify-content-center">
                                <label for="" class="h3 text-white titleDesafio" >Desafío ThinkShield</label>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col col_desafios">
                <div class="backgroundDesafiosInactive d-flex justify-content-center align-items-end pb-3">
                    <div class="row row_svgSpecial" style="position: absolute;  top: -30px;">
                        <div class="col">
                            <img src="{{url('/svg/special.svg')}}" class="w-100 svgSpecial" alt="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio">Desafío Laptops</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col  col_desafios">
                <div class="backgroundDesafiosInactive d-flex justify-content-center align-items-end pb-3">
                    <div class="row">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio">Desafío Tablets</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mx-auto d-flex justify-content-between">
            <div class="col col_desafios">
                <div class="backgroundDesafiosInactive d-flex justify-content-center align-items-end pb-3">
                    <div class="row row_svgSpecial" style="position: absolute;  top: -30px;">
                        <div class="col">
                            <img src="{{url('/svg/special.svg')}}" class="w-100 svgSpecial" alt="">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio">Desafío Laptops</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col  col_desafios">
                <div class="backgroundDesafiosInactive d-flex justify-content-center align-items-end pb-3">
                    <div class="row">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio">Desafío Tablets</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col  col_desafios">
                <div class="backgroundDesafiosInactive d-flex justify-content-center align-items-end pb-3">
                    <div class="row">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio">Desafío Tablets</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--

        <div class="row pr-0 mr-0">
            <div class="col">
                <nav aria-label="Page navigation example">
                    <ul class="pagination justify-content-center">
                      
                      <li class="page-item"><a class="page-link" href="javascript:;">1</a></li>
                      <li class="page-item active"><a class="page-link" href="javascript:;">2</a></li>
                      <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                     
                    </ul>
                  </nav>
            </div>
        </div>

    -->

        <!-- Modal ¿ESTA SEGURO SALIR DE LA CLASE? -->
<div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-2">
                    <img src="{{url('svg/bombillo.svg')}}" alt="">
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <label for="" class="lead text-center h3" style="color: #8246AF; font-weight: bold" >DESAFIO THINKSHIELD</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5 my-3">
                    <p class="text-center fa-lg" style="line-height: 1.5em;">Inicia este desafio y supéralo respondiendo correctamente cada pregunta, tendrás 30 minutos para lograrlo. <br> <br>
                        ¿Estas listo para comenzar?</p>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <a href="{{route('desafios.prueba')}}"  class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                    INICIAR PRUEBA
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>
       
        



<style>

.backgroundModal{
        background-image: url('/img/home.png');
        background-attachment: fixed;
        background-size: cover;
        background-repeat: no-repeat;
    }

    @media (min-width: 1920px){
            .backgroundDesafios{
            background-image: url('/img/desafios_2.png'); 
                background-size:100% 100%;
                background-repeat: no-repeat;
                height: 230px;
                border-radius: 20px !important;
        }

        .backgroundDesafiosInactive{
            background-image: url('/img/desafios_3.png'); 
            background-size:100% 100%;
                background-repeat: no-repeat;
                height: 230px;
                border-radius: 20px !important;
        }

        .titleDesafio{
        font-weight: bold;
        }

        .row_firtscard{
            margin-top: 3%;
            margin-bottom: 5%;
        }

        .svgSpecial{
            width: 80% !important;
        }

        .row_svgSpecial{
            right: -30px !important;
        }

    }


    @media (max-width: 1366px){
            .backgroundDesafios{
            background-image: url('/img/desafios_2.png'); 
            background-size:100% 100%;
                background-repeat: no-repeat;
                height: 150px;
                border-radius: 20px !important;
        }

        .backgroundDesafiosInactive{
            background-image: url('/img/desafios_3.png'); 
            background-size:100% 100%;
                background-repeat: no-repeat;
                height: 150px;
                border-radius: 20px !important;
        }

        .titleDesafio{
        font-weight: bold;
        font-size: 1.3em;
        }

        .titleback_desafios{
            font-size: 1.2em;
        }

        .subtitledback_desafios{
            font-size: 1em;
        }

        .svgSpecial{
            width: 60% !important;
        }

        .row_svgSpecial{
            right: -60px !important;
        }

        .row_firtscard{
            margin-top: 2%;
            margin-bottom: 5%;
        }
    }


    @media (max-width: 720px){
        .col_desafios{
            min-width: 100% !important;
            margin-bottom: 5% !important;
        }

    }
    

</style>
@endsection