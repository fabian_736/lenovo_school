@extends('layouts.home.app')
@section('content')


        <div class="row m-0 p-0">
            <div class="col-1 d-flex justify-content-center align-items-center pt-3">
            <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
            </div>
            <div class="col">
                <div class="row">
                    <div class="col p-0">
                        <label for="" class="h3 text_title"><span style="font-weight: bold">MI PERFIL</label>
                    </div>
                </div>
            </div>
        </div>




        <div class="row mx-auto">
            <div class="col ">
                <div class="card card-body cardbody_principal" style="background-image: url('/img/background_perfilcard.png'); 
                background-attachment: fixed;
                background-size: cover;
                background-repeat: no-repeat; border-radius: 20px;">
                    <div class="row">
                        <div class="col-8 col_one_insignias">
                            <div class="card card-body p-0 pt-3 card_margin_one" style="border-radius: 20px;">
                                <div class="row">
                                    <div class="col ">
                                        <div class="row-reverse">
                                            <div class="col">
                                                @include('layouts.home.utils.progress.index')   
                                            </div>
                                            <div class="col d-flex justify-content-center">
                                                <a href="javascript:;" data-toggle="modal" data-target="#myModal" class="btn px-5 h5" style="border-radius: 40px; font-weight: bold; background: white; border-width: 2px; border-color: #8246AF; color: #8246AF; border-style: solid">EDITAR FOTO</a>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col d-flex justify-content-center align-items-center">
                                        <div class="row-reverse">
                                            <div class="col">
                                                <label for="" class="h2 mb-5 text_title" style="color: #8246AF; font-weight: bold; text-decoration-line: underline;">Javier Torres</label>
                                            </div>
                                            <div class="col mb-4">
                                                <p class="lead text_description"><span style="font-weight: bold">Rol:</span> Vendedor</p>
                                            </div>
                                            <div class="col mb-4">
                                                <p class="lead text_description"><span style="font-weight: bold">Pais:</span> Perú <img src="{{url('svg/country/peru.svg')}}" alt=""> </p>
                                            </div>
                                            <div class="col mb-4">
                                                <p class="lead text_description"><span style="font-weight: bold">Desafios superados:</span> 6 a 10</p>
                                            </div>
                                            <div class="col mb-4">
                                                <p class="lead text_description"><span style="font-weight: bold">Sesiones asistidas:</span> 6 a 10</p>
                                            </div>
                                            <div class="col mb-4 ">
                                                <div class="row">
                                                    <div class="col-8"><p class="lead text_description"><span style="font-weight: bold" >Posición ranking general:</span> </p>
                                                        </div>
                                                    <div class="col d-flex align-items-center"><div class="d-flex justify-content-center align-items-center h4" style="width: 30px; height: 30px; background: #F5C930; font-weight: bold; border-radius: 60px">
                                                        1
                                                    </div></div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-4 col_two_insignias">
                            <div class="card card-body card_margin_two" style="border-radius: 20px;">
                                <div class="row-reverse">
                                    <div class="col d-flex justify-content-center">
                                        <img src="{{url('svg/medal.svg')}}" alt="">
                                    </div>
                                    <div class="col d-flex justify-content-center my-4 p-1 col_title_insignias">
                                        <label for="" class="h3 text_title" style="color: #8246AF; font-weight: bold;">Insignias obtenidos</label>
                                    </div>
                                    <div class="col p-0">
                                        <div class="col_list_insignias" style="overflow: auto; max-height : 250px; overflow-x: hidden"> 
                                            <ul>
                                                <li class="row mb-3">
                                                   <div class="col-3 p-0 col_svg" >
                                                          <img src="{{url('svg/insignias/master_lenovo.svg')}}" alt="" class="w-100 svg_insignias" style="border-radius: 40px">
                                                   </div>
                                                   <div class="col d-flex jsutify-content-center align-items-center">
                                                        <span class="h4 text_description" style="font-weight: bold">MAESTRO LENOVO</span>
                                                   </div>
                                                </li>
                                                <li class="row mb-3">
                                                    <div class="col-3 p-0 col_svg" >
                                                           <img src="{{url('svg/insignias/disciplina_absolute.svg')}}" alt="" class="w-100 svg_insignias" style="border-radius: 40px">
                                                    </div>
                                                    <div class="col d-flex jsutify-content-center align-items-center">
                                                         <span class="h4 text_description" style="font-weight: bold">DISCIPLINA ABSOLUTA</span>
                                                    </div>
                                                 </li>
                                                 <li class="row mb-3">
                                                    <div class="col-3 p-0 col_svg" >
                                                           <img src="{{url('svg/insignias/master_lenovo.svg')}}" alt="" class="w-100 svg_insignias" style="border-radius: 40px">
                                                    </div>
                                                    <div class="col d-flex jsutify-content-center align-items-center">
                                                         <span class="h4 text_description" style="font-weight: bold">MAESTRO LENOVO</span>
                                                    </div>
                                                 </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="col ">
                                        <hr style="border-width: 2px; border-style: solid">
                                    </div>
                                    <div class="col d-flex justify-content-center">
                                        <a href="javascript:;" class="btn text-white h4 text_description" style="font-weight: bold; border-radius: 40px; background: #7DBE38">
                                            IR A RECOMPENSAS
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <!-- modal subir foto -->
        <div class="modal fade " id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog " role="document">
              <div class="modal-content  backgroundModal">
                <div class="modal-body ">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-end align-items-end">
                            <a href="javascript:;" >
                                <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                            </a>
                        </div>
                        <div class="col mt-5">
                                <span class="file-input btn btn-block btn-file h4" style="background: #8246AF; border-radius: 40px;">
                                    <img src="{{url('svg/image.svg')}}" alt="" class="mx-3" style="width: 10%"> SUBIR FOTO <input type="file" accept="image/gif, image/jpeg, image/png" id="input_one">
                                </span>
                                
                        </div>
                        <div class="col my-3">
                            <a href="javascript:;" class="btn h4"  style="color: #8246AF; font-weight: bold; background: white; border-style: solid; border-color: #8246AF; border-width: 2px; width: 100%; border-radius: 40px;">
                                <img src="{{url('svg/camara.svg')}}" alt="" class="mx-3" style="width: 10%"> Tomar foto
                            </a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>


          <!-- segundo modal subir foto -->
          <div class="modal fade " id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog " role="document">
              <div class="modal-content  backgroundModal">
                <div class="modal-body ">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-end align-items-end">
                            <a href="javascript:;" >
                                <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                            </a>
                        </div>
                        <div class="col mt-5 d-flex justify-content-center">
                            <div id="preview_image" style=" width: 60%; height: 250px;">
                            </div>
                        </div>
                        <div class="col my-5">
                            <div class="row">
                                <div class="col-6">
                                    <a href="javascript:;" data-toggle="modal" data-target="#myModal3" data-dismiss="modal" class="btn h4 text-white"  style="font-weight: bold; background: #8246AF; border-style: solid; border-radius: 40px;">
                                        Publicar foto
                                    </a>
                                </div>
                                <div class="col-6">
                                    <span class="file-input btn btn-block btn-file h4" style="background: #8246AF; border-radius: 40px; font-weight: bold">
                                    CAMBIAR FOTO<input type="file" id="input_two" accept="image/gif, image/jpeg, image/png">
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>

         <!-- tercer modal subir foto -->
         <div class="modal fade " id="myModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog " role="document">
              <div class="modal-content  backgroundModal">
                <div class="modal-body ">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-end align-items-end">
                            <a href="javascript:;" >
                                <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                            </a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center my-5">
                            <img src="{{url('svg/image_purple.svg')}}" alt="">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <h3 class="text-center" style="font-weight: bold">¡FELICIDADES!</h3>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <label for="" class="lead text-center" style="color: black" >Su foto se a cambiado éxitosamente</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center my-5">
                            <a href="javascript:;" data-toggle="modal" data-target="#myModal4" data-dismiss="modal" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                                continuar
                            </a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>

         <!-- cuarto modal subir foto -->
         <div class="modal fade " id="myModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog " role="document">
              <div class="modal-content  backgroundModal">
                <div class="modal-body ">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-end align-items-end">
                            <a href="javascript:;" >
                                <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                            </a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mt-5 mb-3">
                            <img src="{{url('svg/insignias/master_lenovo.svg')}}" alt="" class="w-50">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <h3 class="text-center" style="font-weight: bold">!ENHORABUENA!</h3>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <label for="" class="lead text-center" style="color: black" >Has ganado la insignia de <br> <span style="font-weight: bold">Maestro Lenovo.</span> <br><br>
                                Conocé cuales son los beneficios 
                                por este logró</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center my-5">
                            <a href="javascript:;" data-dismiss="modal" aria-label="Close" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                                IR A RECOMPENSAS
                            </a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
        </div>

          <style>

            .backgroundModal{
                    background-image: url('/img/home.png');
                    background-attachment: fixed;
                    background-size: cover;
                    background-repeat: no-repeat;
                }

           
              #preview_image{
                background-image: url('https://electronicssoftware.net/wp-content/uploads/user.png');
                background-size: 100% 100%;
                background-repeat: no-repeat;
                border-radius: 100%;
                box-shadow: inset 0 0 0 13px #8146afcc;


              }

              @media (max-width: 1366px){
                .text_title{
                    font-size: 1.5em !important;
                }

                .text_description{
                    font-size: 1em !important;
                }

                .col_title_insignias{
                    margin: 0 !important;
                }

                .svg_insignias{
                    width: 100% !important;
                }

                .col_two_insignias{
                    min-width: 40% !important;
                    max-width: 40% !important;
                }

                .col_one_insignias{
                    min-width: 60% !important;
                    max-width: 60% !important;
                }

                .col_svg{
                    min-width: 15% !important;
                    max-width: 15% !important;
                }

                .col_list_insignias{
                    max-height: 150px !important;
                }

                .card_margin_one{
                    margin: 0% !important;
                }


                .card_margin_two{
                    margin: 0% !important;
                }

                .cardbody_principal{
                    margin-bottom: 0% !important;
                }
              }

              @media (max-width: 1200px){
                .col_one_insignias{
                    min-width: 100% !important;
                    margin-bottom: 5%;
                }
                .col_two_insignias{
                    min-width: 100% !important;
                }

              }
  
              
          </style>

<script>
    document.getElementById("input_one").onchange = function() {
        $('#myModal').modal('hide');
        $('#myModal2').modal('show');

        var url = URL.createObjectURL(this.files[0]);

        $('#canvas').css({
            'background-image': 'url(" '+ url +' ")',
        })

        $('#preview_image').css({
            'background-image': 'url(" '+ url +' ")',
        })


        }
</script>

<script>
        document.getElementById("input_two").onchange = function() {
        $('#myModal').modal('hide');
        $('#myModal2').modal('show');

        var url = URL.createObjectURL(this.files[0]);

        $('#canvas').css({
            'background-image': 'url(" '+ url +' ")',
        })

        $('#preview_image').css({
            'background-image': 'url(" '+ url +' ")',
        })


        }
</script>



@endsection
