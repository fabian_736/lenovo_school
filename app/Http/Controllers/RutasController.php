<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\RegisterRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RutasController extends Controller
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    // RUTAS AUTH TOKEN
    public function verify_token(){
        return view('auth.token.index');
    }

    public function error_token(){
        return view('auth.token.error');
    }



    // RUTAS AUTH STUDENT
    public function login_student(){
        return view('auth.student.index');
    }

    public function auth_student(Request $request){
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
            $request->session()->regenerate();
            return redirect()->route('tutorial.index');
        };
        return redirect()
                ->intended('login_S');
    }

    public function date_student(){
        return view('auth.student.date');
    }

    public function register_student(RegisterRequest $request){
        try{
            DB::beginTransaction();
            $data = $request->all();
            if($request->hasFile('file')){
                $file = $request->file('file');
                $name = time().$file->getClientOriginalName();
                $file->move(public_path().'/imgs/users/', $name);
                $data["file"] = $name;
            }
            $data["token_reset"] = generateTokenResetPassword();
            $user = $this->user::create($data);

            if(!$user) return redirect()->back();

            \Mail::to($user->email)->send(new \App\Mail\ResetPasswordEmail($user));
            DB::commit();

            return redirect()->route('student.firts_restore');
        }catch(\Exception $e){
            DB::rollback();
        }
    }

    public function firts_restore_student(){
        return view('auth.student.firts_restore');
    }

    public function restore_student($token){
        return view('auth.student.restore', compact('token'));
    }

    public function restore_password_student(ResetPasswordRequest $request){
        $user = $this->user->where('token_reset', $request->token_reset)->first();
        if(!$user) return redirect()->back();

        $user->password = Hash::make($request->password);
        $user->token_reset = null;
        $user->save();

        return redirect()->route('student.login')->with('message', 'Usuario creado correctamente');
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect()->to('/');
    }

    // RUTAS AUTH TEACHER
    public function login_teacher(){
        return view('auth.teacher.index');
    }

    public function date_teacher(){
        return view('auth.teacher.date');
    }

    public function restore_teacher(){
        return view('auth.teacher.restore');
    }



    // RUTAS TUTORIAL
    public function tutorial(){
        return view('tutorial.index');
    }



    // RUTAS HOME
    public function home(){
        return view('portal.index');
    }



    // RUTAS CLASES EN VIVO
    public function live(){
        return view('live.index');
    }

    public function session_live(){
        return view('live.session_live');
    }



    // RUTAS CLASES EN VIVO
    public function class_save(){
        return view('class_save.index');
    }



     // RUTAS DESAFIOS
     public function desafios(){
        return view('desafios.index');
    }

    public function mis_desafios(){
        return view('desafios.mis_desafios');
    }

    public function prueba(){
        return view('desafios.prueba');
    }



      // RUTAS GALERIAS
    public function galeria(){
        return view('galeria.index');
    }

    public function video(){
        return view('galeria.video');
    }



      // RUTAS MI PERFIL
      public function perfil(){
        return view('perfil.index');
    }



    // RUTAS MI RANKING
    public function ranking(){
        return view('ranking.index');
    }



    // RUTAS MI RECOMPENSA
    public function reward(){
        return view('reward.index');
    }


    public function premio(){
        return view('reward.premio');
    }

    public function formulario(){
        return view('reward.formulario');
    }





}
