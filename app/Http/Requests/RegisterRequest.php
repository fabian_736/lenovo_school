<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"                  => "required|string|min:3",
            "lastname"              => "required|string|min:3",
            "email"                 => "required|string|min:3|unique:users,email",
            "company"               => "required|string",
            "position"              => "required|string",
            "channel"               => "required|string",
            "cellphone_number"      => "required|string",
            "file"                  => "mimes:jpeg,jpg,png|nullable"
        ];
    }

    public function messages(){
        return  [
            "name.required"                     => "Ingrese Nombre",
            "lastname.required"                 => "Ingrese Apellido",
            "email.unique"                      => "El Email ya se encuentra registrado",
            "email.required"                    => "Ingrese un Email",
            "company.required"                  => "Ingrese la Empresa a la que pertenece",
            "position.required"                 => "Ingrese el Cargo",
            "channel.required"                  => "Ingrese un Canal",
            "cellphone_number.required"         => "Ingrese número de teléfono",
            "file.mimes"                        => "El archivo debe ser de tipo jpeg, jpg o png"
        ];
    }

}
