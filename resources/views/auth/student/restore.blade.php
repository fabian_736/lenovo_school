@extends('layouts.auth.app')
@section('content')

<body class="body body_login_restore">


<div class="row d-flex align-items-center row_login_restore">
    <div class="card form_login_restore overflow-hidden col-10 col-sm-10 col-md-4 col-lg-4 ml-5 d-flex justify-content-center" >
            <div class="row-reverse ">
                <div class="col d-flex justify-content-center align-items-center">
                    <img class="img-responsive_login_restore" src="{{url('img/logo.png')}}" alt="" >
                </div>
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <h3 for="" class="title_login_restore fa-gl">Reestablece tu contraseña</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center mb-3">
                    <label for="" class="title2_login_restore fa-md">Ingresa una combinación de 8 carácteres mínimo</label>
                </div>
                @include('errors.index')
                <form action=" {{ route('student.restore_password') }} " method="POST">
                    @csrf
                    <input type="hidden" name="token_reset" value={{$token}}>
                    <div class="col">
                        <input type="text" name="password" class="form-control my-icon" placeholder="Nueva contraseña" value="maironurielesnavas">
                    </div>
                    <div class="col my-5">
                        <input type="text" name="password_confirmation" class="form-control my-icon2" placeholder="Repite contraseña" value="maironurielesnavas">
                    </div>

                    <div class="col-8 mx-auto">
                        <button class="btn bottom_login_restore form-control text-white" type="submit">INGRESAR</button>
                    </div>
                </form>
            </div>
    </div>

</div>


@endsection

