 <!-- HEAD -->
@include('layouts.home.components.head')
<!-- END HEAD -->

<div class="wrapper ">

    <!-- SIDEBAR -->
    @include('layouts.home.components.sidebar')
    <!-- END SIDEBAR -->


    <div class="main-panel">

        <!-- NAVBAR -->
        @include('layouts.home.components.navbar')
        <!-- END NAVBAR -->


        <div class="content">
          <div class="content">
            <div class="container-fluid">
              <div class="row">
                <div class="col-md-11">
                  <div class="card m-0 " style="background: transparent; box-shadow: none;">
                    <div class="card-body p-0 cardbody_home" style=" min-height: 75vh; max-height: 75vh; ">

                      @yield('content')

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>


        <footer class="footer fixed-bottom bg-dark">
          <div class="container-fluid">
              <div class="col">
                <a href="https://www.creative-tim.com/" class="text-white text_footer"  >
                  Términos y condiciones | Preguntas frecuentes
                </a>
              </div>
            </div>
        </footer>

        <style>
           @media only screen and (max-width: 1366px) {
            .footer{
              padding-top: 3px;
              padding-bottom: 3px;
            }

             .cardbody_home{
              min-height: 80vh !important;
              max-height: 80vh !important;
              overflow: auto;
             }
           }

           @media only screen and (max-width: 800px) {
             .footer{
               padding: 0 !important;
             }
              .text_footer{
                font-size: 0.8em !important;
              }
           }

      
        </style>

    </div>

</div>


    <!-- PAINT COLORS -->
    @include('layouts.home.components.paint')
    <!-- END PAINT COLORS -->


    <!-- END HTML -->
    @include('layouts.home.components.end')
    <!-- END HTML -->

