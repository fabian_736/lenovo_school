<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RutasController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/clear-cache', function () {
    echo Artisan::call('config:clear');
    echo Artisan::call('config:cache');
    echo Artisan::call('cache:clear');
    echo Artisan::call('route:clear');
    echo Artisan::call('route:cache');
    echo Artisan::call('view:cache');
 });

//RUTAS AUTH TOKEN
Route::get('/token',[RutasController::class,'verify_token'])->name('auth.token');
Route::get('/token_error',[RutasController::class,'error_token'])->name('auth.token_error');

//RUTAS AUTH STUDENT
Route::get('/login_S',[RutasController::class,'login_student'])->name('student.login')->middleware("guest");
Route::post('/login_S',[RutasController::class,'auth_student'])->name('student.auth');

Route::get('/datos_S',[RutasController::class,'date_student'])->name('student.date');
Route::post('/datos_S',[RutasController::class,'register_student'])->name('student.register');

Route::get('/restaurar_S/{token}',[RutasController::class,'restore_student'])->name('student.restore');
Route::post('/restaurar_S',[RutasController::class,'restore_password_student'])->name('student.restore_password');

Route::get('/firts/restaurar_S',[RutasController::class,'firts_restore_student'])->name('student.firts_restore');


//logout
Route::post('/logout',[RutasController::class,'logout'])->name('logout');


//RUTAS AUTH TEACHER
Route::get('/login_T',[RutasController::class,'login_teacher'])->name('teacher.index');
Route::get('/datos_T',[RutasController::class,'date_teacher'])->name('teacher.date');
Route::get('/restaurar_T',[RutasController::class,'restore_teacher'])->name('teacher.restore');


//RUTAS TUTORIAL
Route::get('/tutorial',[RutasController::class,'tutorial'])->name('tutorial.index');


//RUTAS HOME
Route::get('/home',[RutasController::class,'home'])->name('portal.index')->middleware('auth');


//RUTAS LIVE
Route::get('/live',[RutasController::class,'live'])->name('live.index');
Route::get('/live/session_live',[RutasController::class,'session_live'])->name('session_live.index');
Route::get('live/class_save',[RutasController::class,'class_save'])->name('class_save.index');


//RUTAS DESAFIOS
Route::get('/desafios',[RutasController::class,'desafios'])->name('desafios.index');
Route::get('/desafios/mis_desafios',[RutasController::class,'mis_desafios'])->name('desafios.mis_desafios');
Route::get('/desafios/prueba',[RutasController::class,'prueba'])->name('desafios.prueba');


//RUTAS DESAFIOS
Route::get('/galeria',[RutasController::class,'galeria'])->name('galeria.index');
Route::get('/galeria/video',[RutasController::class,'video'])->name('galeria.video');


//RUTAS PERFIL
Route::get('/perfil',[RutasController::class,'perfil'])->name('perfil.index');


//RUTAS RANKING
Route::get('/ranking',[RutasController::class,'ranking'])->name('ranking.index');


//RUTAS REWARD
Route::get('/reward',[RutasController::class,'reward'])->name('reward.index');
Route::get('/reward/premio',[RutasController::class,'premio'])->name('premio.index');
Route::get('/reward/formulario',[RutasController::class,'formulario'])->name('formulario.index');
