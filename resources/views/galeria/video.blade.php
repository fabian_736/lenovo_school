@extends('layouts.home.app')
@section('content')


        <div class="row mb-3 mr-0">
            <div class="col-1 d-flex justify-content-end align-items-center ">
               <a href="{{route('galeria.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
            </div>
            <div class="col d-flex align-items-center ">
                 <label for="" class="h3 mb-4 titleVideo"> <span style="font-weight: bold">Nombre</span> del video</label>
            </div>
        </div>

        <div class="row mr-0">
            <div class="col-8 col_one">
                <div class="row-reverse">
                    <div class="col" >
                        <div style="padding:56.25% 0 0 0;position:relative;">
                            <video  controls style="border-radius: 20px; height: 50vh; object-fit:fill; position:absolute;top:0;left:0;width:100%;height:100%;" poster="{{url('img/desafios/1.png')}}">
                                <source src="{{url('video/1.mp4')}}" type="video/mp4">
                                Your browser does not support the video tag.
                            </video>
                        </div>
                    </div>
                    <div class="col d-flex justify-content-end my-3">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter" class="btn px-5" style="background: #7DBE38; border-radius: 40px;">DESCARGAR</a>
                    </div>
                </div>
            </div>
            <div class="col-3 col_two" style="height: 600px; background-image: url('/img/background_sidebar.png'); 
            background-attachment: fixed;
            background-size: cover;
            background-repeat: no-repeat; border-radius: 20px" >
                <div class="row-reverse">
                    <div class="col py-5" style="border-radius: 20px; ">
                        <div class="row">
                            <div class="col-10 col_box_text">
                                <input type="text" name="" id="" class="form-control" placeholder="Escribe aquí tus preguntas u opiniones...">
                            </div>
                            <div class="col d-flex justify-content-center align-items-center col_bottom_submit">
                                <i class="fas fa-caret-right text-success fa-2x"></i>
                            </div>
                        </div>
                    </div> 
                   
                    <div class="col my-3 col_chat" style="overflow: auto; max-height: 400px;">
                      <ul class="pl-0">
                          <li class="row mb-3 ">
                             <div class="col-3  p-0 pl-2" >
                                    <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                             </div>
                             <div class="col ">
                                 <div class="row-reverse">
                                     <div class="col p-0">
                                         <span class="text-white">Hola, ¿Los beneficios de este producto también se aplican para las Tab p11? Gracias.</span>
                                     </div>        
                                     <div class="col p-0">
                                         <span class="text-white"><a href="javascript:;" style="color: #7DBE38">Ver más</a></span>
                                     </div>
                                 </div>
                             </div>
                          </li>
                          <li class="row mb-3 ">
                            <div class="col-3  p-0 pl-2" >
                                   <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                            </div>
                            <div class="col ">
                                <div class="row-reverse">
                                    <div class="col p-0">
                                        <span class="text-white">Hola, ¿Los beneficios de este producto también se aplican para las Tab p11? Gracias.</span>
                                    </div>        
                                    <div class="col p-0">
                                        <span class="text-white"><a href="javascript:;" style="color: #7DBE38">Ver más</a></span>
                                    </div>
                                </div>
                            </div>
                         </li>
                         <li class="row mb-3 ">
                            <div class="col-3  p-0 pl-2" >
                                   <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                            </div>
                            <div class="col ">
                                <div class="row-reverse">
                                    <div class="col p-0">
                                        <span class="text-white">Hola, ¿Los beneficios de este producto también se aplican para las Tab p11? Gracias.</span>
                                    </div>        
                                    <div class="col p-0">
                                        <span class="text-white"><a href="javascript:;" style="color: #7DBE38">Ver más</a></span>
                                    </div>
                                </div>
                            </div>
                         </li>
                         <li class="row mb-3 ">
                            <div class="col-3  p-0 pl-2" >
                                   <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                            </div>
                            <div class="col ">
                                <div class="row-reverse">
                                    <div class="col p-0">
                                        <span class="text-white">Hola, ¿Los beneficios de este producto también se aplican para las Tab p11? Gracias.</span>
                                    </div>        
                                    <div class="col p-0">
                                        <span class="text-white"><a href="javascript:;" style="color: #7DBE38">Ver más</a></span>
                                    </div>
                                </div>
                            </div>
                         </li>
                         <li class="row mb-3 ">
                            <div class="col-3  p-0 pl-2" >
                                   <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                            </div>
                            <div class="col ">
                                <div class="row-reverse">
                                    <div class="col p-0">
                                        <span class="text-white">Hola, ¿Los beneficios de este producto también se aplican para las Tab p11? Gracias.</span>
                                    </div>        
                                    <div class="col p-0">
                                        <span class="text-white"><a href="javascript:;" style="color: #7DBE38">Ver más</a></span>
                                    </div>
                                </div>
                            </div>
                         </li>
                         <li class="row mb-3 ">
                            <div class="col-3  p-0 pl-2" >
                                   <img src="https://electronicssoftware.net/wp-content/uploads/user.png" alt="" class="w-100" style="border-radius: 40px">
                            </div>
                            <div class="col ">
                                <div class="row-reverse">
                                    <div class="col p-0">
                                        <span class="text-white">Hola, ¿Los beneficios de este producto también se aplican para las Tab p11? Gracias.</span>
                                    </div>        
                                    <div class="col p-0">
                                        <span class="text-white"><a href="javascript:;" style="color: #7DBE38">Ver más</a></span>
                                    </div>
                                </div>
                            </div>
                         </li>
                      </ul>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog " role="document">
              <div class="modal-content backgroundModal">
                <div class="modal-body ">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-end align-items-end">
                            <a href="javascript:;">
                                <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                            </a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center my-5">
                            <img src="{{url('svg/users.svg')}}" alt="">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <h3 class="text-center" style="font-weight: bold">¡FELICIDADES!</h3>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <label for="" class="lead text-center" style="color: black" >Has descargado éxitosamente el video “Nombre del video”</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mt-5">
                            <a href="javascript:;" data-dismiss="modal" aria-label="Close" class="btn text-white" style="background: #8246AF; font-size: 16px;" >
                                SALIR
                            </a>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>


<style>

.backgroundModal{
        background-image: url('/img/home.png');
        background-attachment: fixed;
        background-size: cover;
        background-repeat: no-repeat;
    }

    @media (min-width: 1920px){
            .backgroundDesafios{
            background-image: url('/img/background_video_galeria.png'); 
                background-size:cover;
                background-repeat: no-repeat;
                height: 250px;
                border-radius: 20px !important;
        }

        .backgroundDesafiosInactive{
            background-image: url('/img/background_video_galeria.png'); 
                background-size:cover;
                background-repeat: no-repeat;
                height: 250px;
                border-radius: 20px !important;
        }

        .titleDesafio{
        font-weight: bold;
    }
    }


    @media (max-width: 1366px){
            .backgroundDesafios{
            background-image: url('/img/background_video_galeria.png'); 
                background-size:cover;
                background-repeat: no-repeat;
                height: 150px;
                border-radius: 20px !important;
        }

        .backgroundDesafiosInactive{
            background-image: url('/img/background_video_galeria.png'); 
                background-size:cover;
                background-repeat: no-repeat;
                height: 150px;
                border-radius: 20px !important;
        }

        .col_two{
            max-height: 400px;
        }

        .titleVideo{
            font-size: 1.2rem;
        }

        .col_chat{
            max-height: 230px !important;
        }

        .col_box_text{
            max-width: 80% !important;
        }

        .col_bottom_submit{
            max-width: 20% !important;
        }
    }

    @media (max-width: 800px){
        .col_two{
            min-width: 80% !important;
            margin: auto !important;
        }

        .col_one{
            min-width: 100% !important;
            margin: auto !important;
        }
    }


  

</style>
@endsection