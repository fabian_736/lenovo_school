@extends('layouts.home.app')
@section('content')


    <div class="row pr-0 mr-0">
        <div class="col-1 d-flex justify-content-end align-items-center">
        <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
        </div>
        <div class="col">
            <div class="row">
                <div class="col">
                    <label for="" class="h3 titleback_desafios"><span style="font-weight: bold"><span style="font-weight: bold">GALERIA</span> | LENOVO</label>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <label for="" class="h5 subtitledback_desafios">Repasa y comparte con otros vededores tus opiniones, dudas y/o tips sobre lo aprendido.</label>
                </div>
            </div>
        </div>
    </div>

    <div class="row mx-auto d-flex justify-content-between row_firtscard">
        <div class="col col_galeria">
            <a href="{{route('galeria.video')}}">
                <div class="backgroundDesafios d-flex justify-content-center align-items-end">
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white text-center titleDesafio" >ACCESORIOS-FELIPE CASTRO (Auriculares)</label>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col col_galeria">
            <a href="{{route('galeria.video')}}">
                <div class="backgroundDesafios2 d-flex justify-content-center align-items-end">
                    <div class="row-reverse p-3 ">
                        <div class="col ">
                            <label for="" class="h3 text-white titleDesafio text-center" >ACCESORIOS-FELIPE CASTRO <br> (LENOVO GO)</label>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col col_galeria">
            <a href="{{route('galeria.video')}}">
                <div class="backgroundDesafios3 d-flex justify-content-center align-items-end">
                        <div class="row-reverse p-3">
                            <div class="col d-flex align-items-center justify-content-center">
                                <label for="" class="h3 text-white titleDesafio text-center" >ACCESORIOS-FELIPE CASTRO-(Laptop_Power_Bank)</label>
                            </div>
                        </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row mx-auto d-flex justify-content-between">
        <div class="col col_galeria">
            <div class="backgroundDesafios4 d-flex justify-content-center align-items-end">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio text-center" >ACCESORIOS-FELIPE CASTRO-(THINKPAD DOCKING STATION)</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col col_galeria">
            <div class="backgroundDesafios5 d-flex justify-content-center align-items-end">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio text-center" >ACCESORIOS-FELIPE CASTRO_ (Organizador)</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col col_galeria">
            <div class="backgroundDesafios6 d-flex justify-content-center align-items-end">
                <a href="" data-toggle="modal" data-target="#exampleModalCenter">
                    <div class="row-reverse p-3">
                        <div class="col d-flex align-items-center justify-content-center">
                            <label for="" class="h3 text-white titleDesafio text-center" >EDUCACION - FERNANDO AMADO <br> (QUE ES LANSCHOOL)</label>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>

    <!--

    <div class="row pr-0 mr-0">
        <div class="col">
            <nav aria-label="Page navigation example">
                <ul class="pagination justify-content-center">
                  
                  <li class="page-item"><a class="page-link" href="javascript:;">1</a></li>
                  <li class="page-item active"><a class="page-link" href="javascript:;">2</a></li>
                  <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                 
                </ul>
              </nav>
        </div>
    </div>

-->




<style>

      @media (min-width: 1920px){
              .backgroundDesafios{
              background-image: url('/img/desafios/1.png'); 
                  background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 230px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios2{
              background-image: url('/img/desafios/2.png'); 
                  background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 230px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios3{
              background-image: url('/img/desafios/3.png'); 
                  background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 230px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios4{
              background-image: url('/img/desafios/4.png'); 
                  background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 230px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios5{
              background-image: url('/img/desafios/5.png'); 
                  background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 230px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios6{
              background-image: url('/img/desafios/6.png'); 
                  background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 230px;
                  border-radius: 20px !important;
          }
  
  
          .titleDesafio{
          font-weight: bold;
          font-size: 1.3em;
          }
  
          .row_firtscard{
              margin-top: 3%;
              margin-bottom: 5%;
          }
  
          .svgDiploma{
              width: 30% !important;
          }
  
          .row_svgSpecial{
              right: -30px !important;
          }
  
      }
  
  
      @media (max-width: 1366px){
              .backgroundDesafios{
              background-image: url('/img/desafios/1.png'); 
              background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 150px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios2{
              background-image: url('/img/desafios/2.png'); 
              background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 150px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios3{
              background-image: url('/img/desafios/3.png'); 
              background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 150px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios4{
              background-image: url('/img/desafios/4.png'); 
              background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 150px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios5{
              background-image: url('/img/desafios/5.png'); 
              background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 150px;
                  border-radius: 20px !important;
          }

          .backgroundDesafios6{
              background-image: url('/img/desafios/6.png'); 
              background-size:100% 100%;
                  background-repeat: no-repeat;
                  height: 150px;
                  border-radius: 20px !important;
          }
  
          
  
          .titleDesafio{
          font-weight: bold;
          font-size: 1em;
          }
  
          .titleback_desafios{
              font-size: 1.2em;
          }
  
          .subtitledback_desafios{
              font-size: 1em;
          }
  
          .svgDiploma{
              width: 30% !important;
          }
         
          .row_firtscard{
              margin-top: 2%;
              margin-bottom: 5%;
          }

          .cardgreen{
              position: absolute;
              background: #7DBE38;
              opacity: 0.7;
              max-width: 90.5%;
              min-height: 100%;
              border-radius: 20px;
          }
      }

      @media (max-width: 720px){
        .col_galeria{
            min-width: 100% !important;
            margin-bottom: 5% !important;
        }
      
  
  </style>
@endsection