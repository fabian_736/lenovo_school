@extends('layouts.home.app')
@section('content')




<div class="row pr-0 mr-0">
    <div class="col-1 d-flex justify-content-end align-items-center">
       <a href="{{route('desafios.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <label for="" class="h3 title_prueba"><span style="font-weight: bold">DESAFIO</span> THINKSHIELD</label>
            </div>
        </div>
    </div>
</div>

<div class="row mx-auto pr-0 mr-0">

    <!-- NUMERACIÓN ITEMS -->

    <div class="col-1 mt-3 col_list">
      <div class="row-reverse">
          <div class="col-12 d-flex justify-content-center p-0 m-0 ">
            <div style="border-radius: 80px; border-style: solid; border-color: #7DBE38; border-width: 2px; height: 100px; width: 100%"  class="p-2 cronometro" > 
                <div style="border-radius: 60px; background: #7DBE38; border-width: 2px; height: 100%"  class="w-100 d-flex justify-content-center align-items-center" > 
                        <div id="tiempo" class="text-white" style="font-weight: bold">00:00</div>
                </div>
            </div>
          </div>
          <div class="style2" style="overflow: auto; height: 500px">

     
            <div class="col-12 d-flex justify-content-center p-0 m-0" >
                <hr class="m-0 p-0 li_1" style="   border:         none;
                border-left:    3px solid hsla(200, 10%, 50%,100);
                height:         10vh;
                width:          1px;
                border-color:    #7DBE38">

                <div class="d-flex justify-content-center align-items-center text-white li_1_circle" onclick="item_display_1()" style="cursor: pointer; font-weight: bold; width: 60%; height: 50px; background: #7DBE38; border-radius: 60px; position: absolute; margin-top: 20%">1</div>

            </div>
            <div class="col-12 d-flex justify-content-center p-0 m-0 " >
                <hr class="m-0 p-0 li_2" style="   border:         none;
                border-left:    3px solid hsla(200, 10%, 50%,100);
                height:         10vh;
                width:          1px;
                border-color:    #C4BEB6">

                <div class="d-flex justify-content-center align-items-center text-white li_2_circle" onclick="item_display_2()" style="cursor: pointer; font-weight: bold; width: 60%; height: 50px; background: #C4BEB6; border-radius: 60px; position: absolute; margin-top: 20%">2</div>

            </div>
            <div class="col-12 d-flex justify-content-center p-0 m-0" >
                <hr class="m-0 p-0 li_3" style="   border:         none;
                border-left:    3px solid hsla(200, 10%, 50%,100);
                height:         10vh;
                width:          1px;
                border-color:    #C4BEB6">

                <div class="d-flex justify-content-center align-items-center text-white li_3_circle" onclick="item_display_3()" style="cursor: pointer; font-weight: bold; width: 60%; height: 50px; background: #C4BEB6; border-radius: 60px; position: absolute; margin-top: 20%">3</div>

            </div>
            <div class="col-12 d-flex justify-content-center p-0 m-0 " >
                <hr class="m-0 p-0 li_4" style="   border:         none;
                border-left:    3px solid hsla(200, 10%, 50%,100);
                height:         10vh;
                width:          1px;
                border-color:    #C4BEB6">

                <div class="d-flex justify-content-center align-items-center text-white li_4_circle" onclick="item_display_4()" style="font-weight: bold; width: 60%; height: 50px; background: #C4BEB6; border-radius: 60px; position: absolute; margin-top: 20%">4</div>

            </div>
           
         
        </div>
      </div>
                

               
    </div>

    <!-- CUESTIONARIOS -->

    <div class="col">

        <!-- CUESTIONARIO #1 -->

        <div class="item1 row-reverse">
            <div class="col mb-4 ">
                <label for="" class="h3 titleprueba_cuestionario" style="color: black">Responde la pregunta de forma correcta:</label>
            </div>
            <div class="col">
                <ul>
                    <li style="list-style: none" class="mb-5">
                        <div class="form-check form-check-radio">
                            <label class="form-check-label mx-5 lead" style="color: black">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                 Opción 1
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </li>
                    <li style="list-style: none" class="mb-5">
                        <div class="form-check form-check-radio">
                            <label class="form-check-label mx-5 lead" style="color: black">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                Opción 2
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </li>
                    <li style="list-style: none" class="mb-5">
                        <div class="form-check form-check-radio ">
                            <label class="form-check-label mx-5 lead" style="color: black">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                Opción 3
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </li>
                    <li style="list-style: none" class="mb-5">
                        <div class="form-check form-check-radio">
                            <label class="form-check-label mx-5 lead" style="color: black">
                                <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                Opción 4
                                <span class="circle">
                                    <span class="check"></span>
                                </span>
                            </label>
                        </div>
                    </li>
                </ul>
            </div>
        </div>


        <!-- CUESTIONARIO #2 -->

        <div class="item2 row-reverse" style="display: none">
            <div class="col mb-4">
                <label for="" class="h3 " style="color: black">Responde la pregunta de forma correcta:</label>
            </div>
            <div class="col">
                <ul>
                    <li style="list-style: none" class="mb-5">
                       <div class="row">
                           <div class="col">
                               <div class="card card-body p-5 cardprueba cardprueba_responsive" onclick="card_items21()" style="background: #DFDFDF; cursor: pointer;" >
                                   <label for="" class="text-center" style="color: black; font-weight: bold">Opción 1</label>
                               </div>
                           </div>
                           <div class="col">
                                <div class="card card-body p-5 cardprueba2 cardprueba_responsive" onclick="card_items22()" style="cursor: pointer; background: #DFDFDF">
                                    <label for="" class="text-center" style="color: black; font-weight: bold">Opción 2</label>
                                </div>
                            </div>
                       </div>
                    </li>
                    <li style="list-style: none" class="mb-5" >
                        <div class="row">
                            <div class="col">
                                <div class="card card-body p-5 cardprueba3 cardprueba_responsive" onclick="card_items23()" style="cursor: pointer; background: #DFDFDF">
                                    <label for="" class="text-center" style="color: black; font-weight: bold">Opción 3</label>
                                </div>
                            </div>
                            <div class="col">
                                 <div class="card card-body p-5 cardprueba4 cardprueba_responsive" onclick="card_items24()" style="cursor: pointer; background: #DFDFDF">
                                     <label for="" class="text-center" style="color: black; font-weight: bold">Opción 4</label>
                                 </div>
                             </div>
                        </div>
                    </li>
                   
                </ul>
            </div>
        </div>

        
        <!-- CUESTIONARIO #3 -->

        <div class="item3 row-reverse" style="display: none">
            <div class="col mb-4 col_titlecuestionario">
                <label for="" class="h3 titleprueba_cuestionario" style="color: black">Responde la pregunta de forma correcta:</label>
            </div>
            <div class="col">
                <ul>
                    <li class="mb-5 listanswer">
                        <div class="row-reverse">
                          <div class="col">
                                <label for="" class="lead answer_prueba" style="color: black">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id molestie tempor ac.</label>
                          </div>
                          <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-check form-check-radio">
                                            <label class="form-check-label mx-5 lead radiotext_prueba" style="color: black">
                                                <input class="form-check-input " type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                                Opción 1
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-check form-check-radio">
                                            <label class="form-check-label mx-5 lead radiotext_prueba" style="color: black">
                                                <input class="form-check-input " type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                                Opción 1
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="mb-5 listanswer">
                        <div class="row-reverse">
                          <div class="col">
                                <label for="" class="lead answer_prueba" style="color: black">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id molestie tempor ac.</label>
                          </div>
                          <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-check form-check-radio">
                                            <label class="form-check-label mx-5 lead radiotext_prueba" style="color: black">
                                                <input class="form-check-input " type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                                Opción 1
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-check form-check-radio">
                                            <label class="form-check-label mx-5 lead radiotext_prueba" style="color: black">
                                                <input class="form-check-input " type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                                Opción 1
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="mb-5 listanswer">
                        <div class="row-reverse">
                          <div class="col">
                                <label for="" class="lead answer_prueba" style="color: black">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Id molestie tempor ac.</label>
                          </div>
                          <div class="col">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-check form-check-radio">
                                            <label class="form-check-label mx-5 lead radiotext_prueba" style="color: black">
                                                <input class="form-check-input " type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                                Opción 1
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-check form-check-radio">
                                            <label class="form-check-label mx-5 lead radiotext_prueba" style="color: black">
                                                <input class="form-check-input " type="radio" name="exampleRadios" id="exampleRadios1" value="option1" >
                                                Opción 1
                                                <span class="circle">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>


        <!-- CUESTIONARIO #4 -->

        <div class="item4 row-reverse" style="display: none">
            <div class="col mb-4 col_titlecuestionario">
                <label for="" class="h3 titleprueba_cuestionario" style="color: black">Responde la pregunta de forma correcta:</label>
            </div>
            <div class="col">
                <div class="row ">
                    <div class="col mr-1 col_responsive_movil">
                        <div class="card card-body p-0 cardprueba21" onclick="card_items41()" style="cursor:pointer; background: #DFDFDF; height: 400px; max-height: 400px; border-radius: 20px;">
                            <div class="row">
                                <div class="col d-flex justify-content-center align-items-center img_cardprueba" style="height: 400px; max-height: 400px; border-radius: 20px;">
                                    <img src="{{url('img/prueba_41.png')}}" alt="" class="w-100">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mr-1 col_responsive_movil">
                        <div class="card card-body p-0 cardprueba22" onclick="card_items42()" style="cursor:pointer; background: #DFDFDF; height: 400px; max-height: 400px; border-radius: 20px;">
                            <div class="row">
                                <div class="col d-flex justify-content-center align-items-center img_cardprueba" style="height: 400px; max-height: 400px; border-radius: 20px;">
                                    <img src="{{url('img/prueba_42.png')}}" alt="" class="w-100">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mr-1 col_responsive_movil">
                        <div class="card card-body p-0 cardprueba23" onclick="card_items43()" style="cursor:pointer; background: #DFDFDF; height: 400px; max-height: 400px; border-radius: 20px;">
                            <div class="row">
                                <div class="col  d-flex justify-content-center align-items-center img_cardprueba" style="height: 400px; max-height: 400px; border-radius: 20px;">
                                    <img src="{{url('img/prueba_43.png')}}" alt="" class="w-100">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col mr-1 col_responsive_movil  m-0">
                        <div class="card card-body p-0 cardprueba24" onclick="card_items44()" style="cursor:pointer; background: #DFDFDF; height: 400px; max-height: 400px; border-radius: 20px;">
                            <div class="row">
                                <div class="col d-flex justify-content-center align-items-center img_cardprueba" style="height: 400px; max-height: 400px; border-radius: 20px;">
                                    <img src="{{url('img/prueba_44.png')}}" alt="" class="w-100">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

    </div>


    <div class="col card_part_2">

        <div class="item1 card card-body ">
            <img src="{{url('img/prueba_1.png')}}" alt="" class="w-100">
        </div>

        <div class="item2 card card-body " style="display: none">
            <img src="{{url('img/prueba_2.png')}}" alt="" class="w-100">
        </div>

        <div class="item3 card card-body " style="display: none">
            <img src="{{url('img/prueba_3.png')}}" alt="" class="w-100">
        </div>


    </div>


</div>

<div class="row mx-auto">

    <div class="col d-flex justify-content-start pl-5">
        <a href="javascript:;" class="fa-lg" style="color: #A5A09A; font-weight: bold;">
            Anterior
        </a>
    </div>
    <div class="col d-flex justify-content-end pr-5">
        <a href="javascript:;" data-toggle="modal" data-target="#modal_1" class="fa-lg" style="color: #4C7421; font-weight: bold;">
            Siguiente
        </a>
    </div>

</div>






@include('desafios.modal.index')

   

<script>
    function card_items21(){
          $('.cardprueba').addClass("activecard");
          $('.cardprueba2').removeClass("activecard");
          $('.cardprueba3').removeClass("activecard");
          $('.cardprueba4').removeClass("activecard");
      };

      function card_items22(){
          $('.cardprueba').removeClass("activecard");
          $('.cardprueba2').addClass("activecard");
          $('.cardprueba3').removeClass("activecard");
          $('.cardprueba4').removeClass("activecard");
      };

      function card_items23(){
          $('.cardprueba').removeClass("activecard");
          $('.cardprueba2').removeClass("activecard");
          $('.cardprueba3').addClass("activecard");
          $('.cardprueba4').removeClass("activecard");
      };

      function card_items24(){
          $('.cardprueba').removeClass("activecard");
          $('.cardprueba2').removeClass("activecard");
          $('.cardprueba3').removeClass("activecard");
          $('.cardprueba4').addClass("activecard");
      };
</script>

<script>
  function card_items41(){
        $('.cardprueba21').addClass("activecard");
        $('.cardprueba22').removeClass("activecard");
        $('.cardprueba23').removeClass("activecard");
        $('.cardprueba24').removeClass("activecard");
    };

    function card_items42(){
        $('.cardprueba21').removeClass("activecard");
        $('.cardprueba22').addClass("activecard");
        $('.cardprueba23').removeClass("activecard");
        $('.cardprueba24').removeClass("activecard");
    };

    function card_items43(){
        $('.cardprueba21').removeClass("activecard");
        $('.cardprueba22').removeClass("activecard");
        $('.cardprueba23').addClass("activecard");
        $('.cardprueba24').removeClass("activecard");
    };

    function card_items44(){
        $('.cardprueba21').removeClass("activecard");
        $('.cardprueba22').removeClass("activecard");
        $('.cardprueba23').removeClass("activecard");
        $('.cardprueba24').addClass("activecard");
    };
</script>






  <script>

      function item_display_1(){
          $('.item1').show();
          $('.item2').hide();
          $('.item3').hide();
          $('.item4').hide();
          $('.li_1').css({
              'border-color' : '#7DBE38'
          });
          $('.li_1_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_2').css({
              'border-color' : '#C4BEB6'
          });
          $('.li_2_circle').css({
              'background' : '#C4BEB6'
          }); 
          $('.li_3').css({
              'border-color' : '#C4BEB6'
          });
          $('.li_3_circle').css({
              'background' : '#C4BEB6'
          }); 
          $('.li_4').css({
              'border-color' : '#C4BEB6'
          });
          $('.li_4_circle').css({
              'background' : '#C4BEB6'
          });
          $('.card_part_2').show();
      }

      function item_display_2(){
          $('.item1').hide();
          $('.item2').show();
          $('.item3').hide();
          $('.item4').hide();
          $('.li_1').css({
              'border-color' : '#7DBE38'
          });
          $('.li_1_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_2').css({
              'border-color' : '#7DBE38'
          });
          $('.li_2_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_3').css({
              'border-color' : '#C4BEB6'
          });
          $('.li_3_circle').css({
              'background' : '#C4BEB6'
          }); 
          $('.li_4').css({
              'border-color' : '#C4BEB6'
          });
          $('.li_4_circle').css({
              'background' : '#C4BEB6'
          });
          $('.card_part_2').show();
      }

      function item_display_3(){
          $('.item1').hide();
          $('.item2').hide();
          $('.item3').show();
          $('.item4').hide();
          $('.li_1').css({
              'border-color' : '#7DBE38'
          });
          $('.li_1_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_2').css({
              'border-color' : '#7DBE38'
          });
          $('.li_2_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_3').css({
              'border-color' : '#7DBE38'
          });
          $('.li_3_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_4').css({
              'border-color' : '#C4BEB6'
          });
          $('.li_4_circle').css({
              'background' : '#C4BEB6'
          }); 
          $('.card_part_2').show();
      }

      function item_display_4(){
          $('.item1').hide();
          $('.item2').hide();
          $('.item3').hide();
          $('.item4').show();
          $('.li_1').css({
              'border-color' : '#7DBE38'
          });
          $('.li_1_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_2').css({
              'border-color' : '#7DBE38'
          });
          $('.li_2_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_3').css({
              'border-color' : '#7DBE38'
          });
          $('.li_3_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.li_4').css({
              'border-color' : '#7DBE38'
          });
          $('.li_4_circle').css({
              'background' : '#7DBE38'
          }); 
          $('.card_part_2').hide();
      
      }

     
    
  </script>


<style>
  .activecard{
      border-style: solid; 
      border-color: #7DBE38;
      border-width: 2px;
  }

  .item2_card:hover{
      border-style: solid; 
      border-color: #7DBE38;
      border-width: 2px;
      cursor: pointer;
    }

    @media (max-width: 1366px){
        .title_prueba{
            font-size: 1.2em;
        }

        .li_1_circle{
            width: 100% !important;
        }

        .li_2_circle{
            width: 100% !important;
        }

        .li_3_circle{
            width: 100% !important;
        }
        .li_4_circle{
            width: 100% !important;
        }

        .cronometro{
            width: 200px !important;
            height: 70px !important;
        }

        #tiempo{
            padding: 5px;
        }

        .style2{
            height: 300px !important;
        }

        .cardprueba_responsive{
            padding: 20px !important;
            margin-top: 0 !important;
        }

        .titleprueba_cuestionario{
            font-size: 1.2em;
        }

        .answer_prueba{
            font-size: 1em;
        }

        .radiotext_prueba{
            font-size: 1em;
        }

        .listanswer{
            margin-bottom: 0 !important;
        }

        .col_titlecuestionario{
            margin-bottom: 0 !important;
        }

        .cardprueba21, .cardprueba22, .cardprueba23, .cardprueba24{
            max-height: 250px !important;
        }

        .img_cardprueba{
            max-height: 250px !important;
        }
    }


    @media (max-width: 800px){
        .col_list{
            min-width: 50% !important;
            margin: auto !important;
        }

        .col_responsive_movil{
            min-width: 100% !important;
            margin: auto !important;
        }

    }
</style>





@endsection


