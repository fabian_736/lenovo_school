
          <div class="counter" data-cp-percentage="50" data-cp-color="rgba(56, 248, 212, 1)">
          </div>


          <style>
              canvas{
                background-image: url('https://electronicssoftware.net/wp-content/uploads/user.png');

background-size: 100% 100%;
background-repeat: no-repeat;
border-radius: 100%;
box-shadow: inset 0 0 0 13px #8146afcc;


              }
          </style>
  

<script>
   document.addEventListener("DOMContentLoaded", function() {

var circleProgress = (function(selector) {
  var wrapper = document.querySelectorAll(selector);
  Array.prototype.forEach.call(wrapper, function(wrapper, i) {
    var wrapperWidth,
      wrapperHeight,
      percent,
      innerHTML,
      context,
      lineWidth,
      centerX,
      centerY,
      radius,
      newPercent,
      speed,
      from,
      to,
      duration,
      start,
      strokeStyle,
      text;

    var getValues = function() {
      wrapperWidth = parseInt(window.getComputedStyle(wrapper).width);
      wrapperHeight = wrapperWidth;
      percent = wrapper.getAttribute('data-cp-percentage');
      innerHTML = '<span class="percentage" style="display: none"><strong>' + percent + '</strong> %</span><canvas id="canvas" class="circleProgressCanvas" width="' + (wrapperWidth * 2) + '" height="' + wrapperHeight * 2 + '"></canvas>';
      wrapper.innerHTML = innerHTML;
      text = wrapper.querySelector(".percentage");
      canvas = wrapper.querySelector(".circleProgressCanvas");
      wrapper.style.height = canvas.style.width = canvas.style.height = wrapperWidth + "px";
      context = canvas.getContext('2d');
      centerX = canvas.width / 2;
      centerY = canvas.height / 2;
      newPercent = 0;
      speed = 1;
      from = 0;
      to = percent;
      duration = 1000;
      lineWidth = 50;
      radius = canvas.width / 2 - lineWidth;
      strokeStyle = wrapper.getAttribute('data-cp-color');
      start = new Date().getTime();
    };

    function animate() {
      requestAnimationFrame(animate);
      var time = new Date().getTime() - start;
      if (time <= duration) {
        var x = easeInOutQuart(time, from, to - from, duration);
        newPercent = x;
        text.innerHTML = Math.round(newPercent) + " %";
        drawArc();
      }
    }

    function drawArc() {
      var circleStart = 1.5 * Math.PI;
      var circleEnd = circleStart + (newPercent / 50) * Math.PI;
      context.clearRect(0, 0, canvas.width, canvas.height);
      context.beginPath();
      context.arc(centerX, centerY, radius, circleStart, 4 * Math.PI, false);
      context.lineWidth = lineWidth;
      context.strokeStyle = "#E1E1E1";
      context.stroke();
      context.beginPath();
      context.arc(centerX, centerY, radius, circleStart, circleEnd, false);
      context.lineWidth = lineWidth;
      context.strokeStyle = strokeStyle;
      context.stroke();

    }
    var update = function() {
      getValues();
      animate();
    }
    update();

  });

  function easeInOutQuart(t, b, c, d) {
    if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;
    return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
  }

});

circleProgress('.counter');

});

</script>