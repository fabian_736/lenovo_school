@extends('layouts.auth.app')
@section('content')

<body class="body">
    
   <div class="row-reverse">
       <div class="col mt-5">
            <img src="{{url('img/logo_blanco.png')}}" alt="">
       </div>
       <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 my-5 cardtoken">
            <div class="card">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center my-5">
                        <img src="{{url('svg/warning_green.svg')}}" alt="" class="icon_lenovo">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center mb-3">
                        <label for="" class="h2" >Este usuario no <br> fue encontrado</label>
                    </div>
                    <div class="col">
                        <p class="text-center mx-3 h4" style="font-weight:normal">Por favor revisa las credenciales de <br> Lenovo Partner Hub, en caso de <br> continuar con el problema comunicate <br> con un asesor</p>
                    </div>
                    <div class="col-8 mx-auto my-5">
                        <a href="{{route('student.date')}}" class="btn bottom form-control text-white" >VOLVER A INGRESAR</a>
                    </div>
                </div>
            </div>
       </div>
   </div>

    <style>
        .body{
            background-image: url('/img/background.png');
            background-attachment: fixed;
            background-size: cover;
            background-repeat: no-repeat;
            font-family: Gotham;
        }

        .bottom{
            border-radius: 20px;
            background: #7DBE38;
            font-size: 20px;
        }

        .card{
            border-radius: 20px;
        }

       @media (min-width: 768px) and (max-width: 1300px){
        .cardtoken{
            min-width: 100%;
        }
       }

       @media (max-width: 1366px){
        .icon_lenovo{
            max-width: 50%;
        }
    }
    </style>


@endsection