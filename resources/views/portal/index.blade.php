@extends('layouts.home.app')
@section('content')

    @include('layouts.home.utils.tutorial.index')

    <div class="row-reverse">
        <div class="col" >
            <h3 class="title_home">BIENVENIDO A <b style="font-weight: bold;">LENOVO SCHOOL</b></h3>
        </div>
        <div class="col mt-4 mb-5 col-carrousel">
            <div id="carouselExampleIndicators" class="carousel slide " data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active mt-5 li_indicador"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1" class="mt-5 li_indicador" ></li>
                </ol>
                <div class="carousel-inner itemCarrousel">
                    <div class="carousel-item active">
                        <img class="d-block w-100 img_carrousel" src="{{url('img/generica1.png')}}" alt="First slide" >
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100 img_carrousel" src="{{url('img/generica2.png')}}" alt="Second slide">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row mx-auto ">
        <div class="col col_carditem_home">
            <div class="d-flex justify-content-center align-items-end carditem_home" onclick="window.location.href = '{{route('live.index')}}'" >
                <div class="row-reverse mb-5">
                    <div class="col d-flex justify-content-center">
                        <h3 class="text-white font-weight-bold title_carditem_home">Clases en vivo</h3>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <h4 class="text-white subtitled_carditem_home">Entrena tus habilidades</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col_carditem_home d-flex justify-content-center">
            <div class="d-flex justify-content-center align-items-end carditem_home_2" onclick="window.location.href = '{{route('desafios.index')}}'">
                <div class="row-reverse mb-5">
                    <div class="col d-flex justify-content-center">
                        <h3 class="text-white font-weight-bold title_carditem_home">Desafios Lenovo</h3>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <h4 class="text-white subtitled_carditem_home">Prueba tus conocimientos</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="col col_carditem_home d-flex justify-content-end">
            <div class="d-flex justify-content-center align-items-end carditem_home_3" onclick="window.location.href = '{{route('galeria.index')}}'">
                <div class="row-reverse mb-5">
                    <div class="col d-flex justify-content-center">
                        <h3 class="text-white font-weight-bold title_carditem_home">Galería</h3>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <h4 class="text-white subtitled_carditem_home">Refresca tus aprendizajes</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <style>
        
        .carditem_home:hover{
            border-style: solid;
            border-width: 5px;
            border-color: #7DBE38;
            cursor: pointer;
        }

        .carditem_home_2:hover{
            border-style: solid;
            border-width: 5px;
            border-color: #7DBE38;
            cursor: pointer;
        }

        .carditem_home_3:hover{
            border-style: solid;
            border-width: 5px;
            border-color: #7DBE38;
            cursor: pointer;
        }

        .li_indicador{
            cursor: pointer;
            width: 10px !important;
            height: 10px !important;
            border-radius: 40px;
          }

          .title_carditem_home{
                font-weight: bold;
                font-size: 35px;
                line-height: 38px;
                text-align: center;
            }

            .subtitled_carditem_home{
                font-style: normal;
                font-weight: normal;
                font-size: 20px;
                line-height: 19px;
            }


        @media only screen and (max-width: 1920px) {
          .title_home{
           font-size: 2.5em;
          }

          .img_carrousel{
            max-height:150px !important;
          }

          .carditem_home{
            cursor: pointer;
            min-height: 450px;
            min-width: 85%;
            max-width: 85%;
            border-radius: 20px;
            background-image: url('img/clases_en_vivo_2.png');
            background-size: 100% 100%;
          }

          .carditem_home_2{
            cursor: pointer;
            min-height: 450px;
            min-width: 85%;
            max-width: 85%;
            border-radius: 20px;
            background-image: url('img/desafios_home_2.png');
            background-size: 100% 100% ;
          }

          .carditem_home_3{
            cursor: pointer;
            min-height: 450px;
            min-width: 85%;
            max-width: 85%;
            border-radius: 20px;
            background-image: url('img/galeria_2.png');
            background-size: 100% 100% ;
          }

        }

        @media only screen and (max-width: 1366px) {
            .title_home{
                font-size: 1.8em;
            }

            .img_carrousel{
            max-height:100px !important;
          }

            .title_carditem_home{
                font-weight: bold !important;
                font-size: 25px !important;
                line-height: 38px !important;
            }

            .subtitled_carditem_home{
                font-style: normal;
                font-weight: normal;
                font-size: 16px;
                line-height: 19px;
            }

            .col-carrousel{
                margin-bottom: 3% !important;
            }

            .carditem_home{
            cursor: pointer;
            min-height: 300px;
            min-width: 90%;
            max-width: 90%;
            border-radius: 20px;
            background-image: url('img/clases_en_vivo_2.png');
            background-size: 100% 100%;
          }

          .carditem_home_2{
            cursor: pointer;
            min-height: 300px;
            min-width: 90%;
            max-width: 90%;
            border-radius: 20px;
            background-image: url('img/desafios_home_2.png');
            background-size: 100% 100% ;
          }

          .carditem_home_3{
            cursor: pointer;
            min-height: 300px;
            min-width: 90%;
            max-width: 90%;
            border-radius: 20px;
            background-image: url('img/galeria_2.png');
            background-size: 100% 100% ;
          }

        }

        @media only screen and (max-width: 750px) {

            .title_home{
                text-align: center;
                margin-bottom: 5%;
            }

            .col-carrousel{
                display: none;
            }

            .col_carditem_home{
                min-width: 100% !important;
                display: flex !important;
                justify-content: center !important;
                margin-bottom: 5%;
            }

        }
     </style>




    

@endsection
