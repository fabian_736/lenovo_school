@extends('layouts.home.app')
@section('content')


<div class="row mr-0 ">
    <div class="col-1 d-flex justify-content-end align-items-center">
       <a href="{{route('portal.index')}}" ><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
    </div>
    <div class="col">
        <div class="row">
            <div class="col">
                <label for="" class="h3 title_premio" style="font-weight: bold">PREMIOS</label>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <label for="" class="h5">Canjea tus puntos por maravillosos premios
                </label>
            </div>
        </div>
    </div>
</div>


<div class="row mx-auto">
    <div class="col card card-body" >
        <div class="row style2" style="overflow: auto;  max-height: 50vh !important; min-height: 50vh !important" >
            <div class="col ">
                <li class="row d-flex justify-content-center align-items-center mb-3 ">
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="row d-flex justify-content-center align-items-center mb-3">
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="row d-flex justify-content-center align-items-center mb-3">
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li class="row d-flex justify-content-center align-items-center mb-3">
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-3 mx-auto card card-body m-0 cardpremio" style= "border-radius: 20px">
                        <a href="javascript:;" data-toggle="modal" data-target="#exampleModalCenter">
                            <div class="row">
                                <div class="col-3">
                                    <img src="{{url('img/premio.png')}}" alt="">
                                </div>
                                <div class="col">
                                    <div class="row-reverse">
                                        <div class="col">
                                            <label for="" class="lead description_premio" style="color: white; font-weight:bold">Card Spotify</label>
                                        </div>
                                        <div class="col">
                                            <label for="" class="lead" style="color: white; font-weight:bold">500pts</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
            </div>
        </div>
    </div>
</div>





<!-- Modal ¿ESTA SEGURO SALIR DE LA CLASE? -->
<div class="modal fade " id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
      <div class="modal-content backgroundModal">
        <div class="modal-body ">
            <div class="row-reverse">
                <div class="col d-flex justify-content-end align-items-end">
                    <a href="javascript:;">
                        <i class="far fa-times-circle fa-2x" data-dismiss="modal" aria-label="Close"></i>
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center my-5">
                    <img src="{{url('img/spotify.png')}}" alt="" class="w-25">
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <h3 class="text-center" style="font-weight: bold">Este premio vale</h3>
                </div>
                <div class="col d-flex justify-content-center align-items-center px-5">
                    <label for="" class="lead text-center h1" style="color: black; font-weight: bold;" >550pts</label>
                </div>
                <div class="col d-flex justify-content-center align-items-center mt-5">
                    <a href="{{route('formulario.index')}}" class="btn text-white" style="width: 80%; background: #8246AF; font-size: 16px;" >
                        redimir
                    </a>
                </div>
                <div class="col d-flex justify-content-center align-items-center">
                    <a href="javascript:;" data-dismiss="modal" aria-label="Close" class="btn " style="font-weight: bold; width: 80%; color: #8246AF; background: white; border-style: solid; border-color: #8246AF; border-width: 2px; font-size: 16px;" >
                        Buscar más premios
                    </a>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>




<style>
    .cardpremio{
    background-image: url('/img/background_PERFILCARD.png'); 
                    background-size: 100% 100%;
                    background-repeat: no-repeat;
    }


    @media (max-width: 1366px){
        .title_premio{
            font-size: 1.5em;
        }

        .description_premio{
            font-size: 1em;
        }
    }

    @media (max-width: 800px){
        .title_premio{
            font-size: 1.5em;
        }

        .description_premio{
            font-size: 1em;
        }

        .cardpremio{
            min-width: 100%;
            margin-bottom: 5% !important;
        }
    }


  
</style>


@endsection